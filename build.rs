extern crate bindgen;

use std::process::Command;
use std::env;
use std::path::{PathBuf, Path};

fn main() {
    let target = "DISH_LIB_TARGET_NAME";
    let output = Command::new("make").arg(target).env_clear().output().expect("Error!");

    let lib_path     = String::from_utf8(output.stdout).unwrap();
    let wrapper_path = "c-wrapper.h";

    let lib_dir_path = Path::new(&lib_path).parent().unwrap().to_str().unwrap();
    let lib_name     = Path::new(&lib_path).file_stem().unwrap().to_str().unwrap();

    let binding_path = "bindings.rs";

    println!("cargo:rustc-link-lib={}",  &lib_name[3..]);
    println!("cargo:rustc-link-search={}", lib_dir_path);
    println!("cargo:rerun-if-changed={}", wrapper_path);
    println!("cargo:rerun-if-changed={}", lib_path);

    let bindings = bindgen::Builder::default()
        .header(wrapper_path)
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .generate()
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join(binding_path))
        .expect("Couldn't write bindings!");
}
