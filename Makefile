CUCINADIR=cucina/

CSRC=c_src/
CINC=c_inc/

CFILES=$(shell find $(CSRC) -type f -name '*.c')

DISH_GLOBAL_PARAMS:=\
    INC="$(CINC) $(CSRC)" \
    WARNINGS="all extra no-unused-parameter no-switch no-format-truncation" \
    EXTRACFLAGS="-g"

DISH_LIB_PARAMS:=\
    CFILES="$(CFILES)"\
    NAME=uwui \
    TYPE=STATIC_LIB

DISH_HELLOWORLD_PARAMS:=\
    CFILES="$(CFILES) test/helloworld.c" \
    NAME=helloworld \
    TYPE=EXECUTABLE

DISH_RUST_PARAMS:=\
    TYPE=CMD \
    CMD="cargo build --color always"

DISH_RUST: DISH_LIB

DISHES=LIB HELLOWORLD RUST

include $(CUCINADIR)menu.mk
