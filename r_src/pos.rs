pub struct PosIterator
{
    rect: Rect,
    ind:  usize
}

impl Iterator for PosIterator
{
    type Item = Pos;

    fn next(&mut self) -> Option<Pos>
    {
        let rtn = self.rect.ind_to_pos(self.ind);
        self.ind += 1;

        return rtn;
    }
}

impl PosIterator
{
    fn new(rect: Rect) -> Self { Self { rect: rect, ind: 0 } }
}

#[derive (Debug, Copy, Clone)]
pub struct Size
{
    pub w: i64,
    pub h: i64
}

impl Size
{
    pub fn zero()              -> Self { Self { w: 0, h: 0 } }
    pub fn new(w: i64, h: i64) -> Self { Self { w: w.abs(), h: h.abs() } }

    pub fn from_rect(rect: Rect) -> Self {
        Self::new(rect.width(), rect.height())
    }

    pub fn area(&self) -> usize {
        self.w as usize * self.h as usize
    }

    pub fn engulf(&mut self, oth: Size)
    {
        if oth.w > self.w { self.w = oth.w }
        if oth.h > self.h { self.h = oth.h }
    }

    pub fn iter_from_origin(&self) -> PosIterator {
        Rect::from_size(Pos::zero(), self.clone()).iter_pos()
    }

    pub fn pos_to_ind_from_origin(&self, p: Pos) -> Option<usize> {
        Rect::from_size(Pos::zero(), self.clone()).pos_to_ind(p)
    }

    pub fn ind_to_pos_from_origin(&self, i: usize) -> Option<Pos> {
        Rect::from_size(Pos::zero(), self.clone()).ind_to_pos(i)
    }
}


#[derive (Debug, Copy, Clone)]
pub struct Pos
{
    pub x: i64,
    pub y: i64
}

impl Pos
{
    pub fn zero()              -> Self { Self { x: 0, y: 0 } }
    pub fn new(x: i64, y: i64) -> Self { Self { x: x, y: y } }

    pub fn add(a: Pos, b: Pos) -> Self {
        Self { x: a.x + b.x, y: a.y + b.y }
    }
    pub fn sub(a: Pos, b: Pos) -> Self {
        Self { x: a.x - b.x, y: a.y - b.y }
    }
}

#[derive (Debug, Copy, Clone)]
pub struct Rect
{
    origin: Pos,
    size:   Size
}

impl Rect
{
    pub fn zero() -> Self { Self { origin: Pos::zero(), size: Size::zero() } }

    pub fn from_size(p: Pos, s: Size) -> Self { Self { origin: p, size: s } }

    pub fn size(&self) -> Size { self.size }

    pub fn width(&self)  -> i64 { self.size.w }
    pub fn height(&self) -> i64 { self.size.h }

    pub fn top(&self)    -> i64 { self.origin.y }
    pub fn bottom(&self) -> i64 { self.origin.y + self.size.h }
    pub fn left(&self)   -> i64 { self.origin.x }
    pub fn right(&self)  -> i64 { self.origin.x + self.size.w }

    pub fn top_left(&self)     -> Pos { Pos::new(self.left(),  self.top()) }
    pub fn top_right(&self)    -> Pos { Pos::new(self.right(), self.top()) }
    pub fn bottom_left(&self)  -> Pos { Pos::new(self.left(),  self.bottom()) }
    pub fn bottom_right(&self) -> Pos { Pos::new(self.right(), self.bottom()) }

    pub fn expand_left (&mut self, n: i64) { self.origin.x -= n; self.size.w += n; }
    pub fn expand_right(&mut self, n: i64) { self.size.w += n; }
    pub fn expand_up   (&mut self, n: i64) { self.origin.y -= n; self.size.h += n; }
    pub fn expand_down (&mut self, n: i64) { self.size.h += n; }

    pub fn engulf(&mut self, pos: Pos) {
        if pos.x >= self.right() {
            self.expand_right(1 + pos.x - self.right());
        }
        if pos.y >= self.bottom() {
            self.expand_down(1 + pos.y - self.bottom());
        }
        if pos.x < self.left() {
            self.expand_left(self.left() - pos.x);
        }
        if pos.y < self.top() {
            self.expand_up(self.top() - pos.y);
        }
    }

    pub fn pos_to_ind(&self, pos: Pos) -> Option<usize>
    {
        if self.contains(pos)
        {
            let relpos = Pos::sub(pos, self.origin);
            return Some((self.width() * relpos.y + relpos.x) as usize);
        }
        else
        {
            return None;
        }
    }

    pub fn ind_to_pos(&self, ind: usize) -> Option<Pos> {
        if ind < self.area() {
            Some(Pos::add(Pos::new(
                ind as i64 % self.width(),
                ind as i64 / self.width()
            ), self.origin))
        } else {
            None
        }
    }

    pub fn contains(&self, pos: Pos) -> bool {
        pos.x >= self.left() && pos.x < self.right() &&
        pos.y >= self.top()  && pos.y < self.bottom()
    }

    pub fn iter_pos(&self) -> PosIterator {
        PosIterator::new(self.clone())
    }

    pub fn area(&self) -> usize { self.size.area() }
}
