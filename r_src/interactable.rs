use crate::err::ErrorInfo;

pub enum InteractionState
{
    Idle,
    Selectable,
    Focused,
    Selected
}

pub trait Interactable
{
    fn press(&self) -> Result<(), ErrorInfo> { Ok(()) }

    fn can_select(&self) -> bool {
        self.can_press() || self.can_focus() || self.can_cursor()
    }

    fn can_press(&self)  -> bool;
    fn can_focus(&self)  -> bool;
    fn can_cursor(&self) -> bool;
}

