use crate::cbind::font::{Font};

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Chr
{
    font:        Font,
    c:           char
}

impl Chr
{
    pub fn new(c: char, font: Font) -> Self { Self { font: font, c: c } }

    pub fn new_space() -> Self { Self::new(' ', Font::new_default()) }

    pub fn get_str(&self) -> String
    {
        let mut buf: [u8; 4] = [0; 4];
        return String::from(self.c.encode_utf8(&mut buf));
    }

    pub fn get_utf8(&self) -> Vec<u8> {
        Vec::from(self.get_str().as_bytes())
    }

    pub fn get_font(&self) -> Font {
        self.font
    }
}

