#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

pub fn new_errmsg() -> uwui_errmsg_t {
    return [0 as u8; UWUI_ERRMSG_LEN as usize]
}

pub trait ErrorResult {
    const ok:  Self;
    const err: Self;

    fn is_ok(&self)  -> bool;
    fn is_err(&self) -> bool;
}

impl ErrorResult for uwui_res_t
{
    const ok:  Self = uwui_res_t_UWUI_OK;
    const err: Self = uwui_res_t_UWUI_ERR;

    fn is_ok(&self) -> bool {
        *self == uwui_res_t_UWUI_OK
    }

    fn is_err(&self) -> bool {
        *self == uwui_res_t_UWUI_ERR
    }
}
