use crate::cbind::clib;
use crate::cbind::clib::ErrorResult;
use crate::err::ErrorInfo;
use std::ptr;


pub struct EventConsumer
{
    ptr: *mut clib::uwui_event_consumer_s,
    cb:  Option<Box<dyn Fn(&Event) -> Result<(),ErrorInfo>>>
}

pub struct EventProducer
{
    ptr: *mut clib::uwui_event_producer_s
}

pub struct EventCtx
{
    ptr: *mut clib::uwui_event_ctx_s
}

pub type EventSpec = clib::uwui_event_spec_s;

pub type Event = clib::uwui_event_s;

impl Event
{
    pub fn get_type(&self) -> u32 { unsafe {
        clib::uwui_event_get_type(self)
    }}

    pub fn get_data<'a>(&'a self) -> &'a [u8] { unsafe {
        std::slice::from_raw_parts(
            clib::uwui_event_get_data(self),
            clib::uwui_event_get_data_len(self) as usize
        )
    }}
}

impl Drop for EventConsumer
{
    fn drop(&mut self) { unsafe { clib::uwui_event_consumer_destroy(self.ptr) }}
}

impl Drop for EventProducer
{
    fn drop(&mut self) { unsafe { clib::uwui_event_producer_destroy(self.ptr) }}
}

impl Drop for EventCtx
{
    fn drop(&mut self) { unsafe { clib::uwui_event_ctx_destroy(self.ptr) }}
}

impl EventCtx
{
    pub fn new() -> Result<Self, ErrorInfo> {
        let mut errmsg = clib::new_errmsg();

        unsafe
        {
            let rtn = Self {
                ptr: clib::uwui_event_ctx_create(errmsg.as_mut_ptr())
            };

            if rtn.ptr.is_null()
            {
                return Err(ErrorInfo::from_errmsg(errmsg));
            }

            return Ok(rtn);
        }
    }

    pub fn dispatch_next(&mut self) -> Result<(), ErrorInfo>
    {
        let mut errmsg = clib::new_errmsg();

        unsafe
        {
            if clib::uwui_event_ctx_dispatch_next(
                self.ptr, errmsg.as_mut_ptr()
            ).is_err()
            {
                return Err(ErrorInfo::from_errmsg(errmsg));
            }
        }

        return Ok(());
    }
}

impl EventSpec
{
    pub fn new(
        typ:      Option<clib::uwui_event_type_t>,
        producer: Option<&EventProducer>
    ) -> Self {
        Self {
            producer: match producer {
                Some(p) => p.ptr,
                None    => ptr::null()
            },
            typ: match typ {
                Some(v) => v,
                None    => clib::uwui_event_type_t_UWUI_EVENT_ANY
            }
        }
    }
}

impl EventConsumer
{
    pub fn new(
        ctx:  &mut EventCtx,
        spec: &EventSpec,
    ) -> Result<Self, ErrorInfo> {
        let mut errmsg = clib::new_errmsg();

        unsafe
        {
            let rtn = Self {
                ptr: clib::uwui_event_consumer_create(
                    ctx.ptr, spec, errmsg.as_mut_ptr()
                ),
                cb: None
            };

            if rtn.ptr.is_null()
            {
                return Err(ErrorInfo::from_errmsg(errmsg));
            }

            return Ok(rtn);
        }
    }

    pub fn set_cb(
        &mut self,
        cb: Box<dyn Fn(&Event) -> Result<(), ErrorInfo>>
    ) -> Result<(), ErrorInfo>
    {
        let mut errmsg = clib::new_errmsg();

        unsafe
        {
            let selfptr: *mut Self = &mut *self;
            if clib::uwui_event_consumer_set_cb(
                self.ptr,
                selfptr as *mut std::ffi::c_void,
                Some(Self::callback_handler),
                errmsg.as_mut_ptr()
            ).is_err()
            {
                return Err(ErrorInfo::from_errmsg(errmsg));
            }
        }

        self.cb = Some(cb);

        return Ok(());
    }

    fn do_cb(&mut self, event: &Event) -> Result<(), ErrorInfo>
    {
        match &self.cb {
            Some(cb) => cb(event),
            None     => Err(ErrorInfo::from_str("Callback is not set up."))
        }
    }

    extern fn callback_handler(
        ptr:     *mut   std::ffi::c_void,
        event:   *const clib::uwui_event_s,
        errptr:  *mut   u8
    ) -> clib::uwui_res_t
    {
        unsafe
        {
            let errmsg: *mut clib::uwui_errmsg_t = *errptr as *mut _;

            // TODO: EVENT
            match (*(ptr as *mut Self)).do_cb(& *event)
            {
            Err(einfo) =>
                {
                    einfo.to_errmsg(&mut *errmsg);
                    return clib::uwui_res_t::err;
                }
            Ok(()) =>
                {
                    return clib::uwui_res_t::ok;
                }
            }
        }
    }
}

impl EventProducer
{
    pub fn new(ctx: &mut EventCtx) -> Result<Self, ErrorInfo> {
        let mut errmsg = clib::new_errmsg();

        unsafe
        {
            let rtn = Self {
                ptr: clib::uwui_event_producer_create(
                    ctx.ptr, errmsg.as_mut_ptr()
                )
            };

            if rtn.ptr.is_null()
            {
                return Err(ErrorInfo::from_errmsg(errmsg));
            }

            return Ok(rtn);
        }
    }

    pub fn push(
        &mut self,
        typ: clib::uwui_event_type_t,
        data: &[u8],
    ) -> Result<(), ErrorInfo>
    {
        let mut errmsg = clib::new_errmsg();

        unsafe
        {
            if clib::uwui_event_push(
                    self.ptr,
                    typ,
                    data.as_ptr(), data.len() as u64,
                    errmsg.as_mut_ptr()
                ).is_err()
            {
                return Err(ErrorInfo::from_errmsg(errmsg));
            }
        }

        return Ok(());
    }
}

