use crate::cbind::clib;

#[derive (Debug, Copy, Clone, PartialEq)]
pub enum Colour
{
    NoColour,
    Family16    { num: u8, bright: bool },
    Family256   { num: u8 },
    Family24Bit { r: u8, g: u8, b: u8 }
}

impl Colour
{
    fn to_colour_t(&self) -> u32
    {
        unsafe
        {
            return match self {
                Self::NoColour =>
                    clib::uwui_colour_default(),

                Self::Family16 { num, bright } =>
                    clib::uwui_colour_16(*num, *bright),

                Self::Family256 { num } =>
                    clib::uwui_colour_256(*num),

                Self::Family24Bit { r, g, b } =>
                    clib::uwui_colour_24bit(*r, *g, *b)
            };
        }
    }
}

#[derive (Debug, Copy, Clone, PartialEq)]
pub struct FontAttrs
{
    pub bold:        bool,
    pub faint:       bool,
    pub italic:      bool,
    pub under:       bool,
    pub blink:       bool,
    pub fblink:      bool,
    pub rev:         bool,
    pub hide:        bool,
    pub strike:      bool
}

impl FontAttrs
{
    pub fn new_default() -> Self {
        Self {
            bold:        false,
            faint:       false,
            italic:      false,
            under:       false,
            blink:       false,
            fblink:      false,
            rev:         false,
            hide:        false,
            strike:      false
        }
    }

    fn to_font_attr_t(&self) -> u16 {
        let mut rtn: u16 = 0;

        if self.bold   { rtn |= clib::UWUI_FONT_BOLD       as u16; }
        if self.faint  { rtn |= clib::UWUI_FONT_FAINT      as u16; }
        if self.italic { rtn |= clib::UWUI_FONT_ITALIC     as u16; }
        if self.under  { rtn |= clib::UWUI_FONT_UNDER      as u16; }
        if self.blink  { rtn |= clib::UWUI_FONT_BLINK      as u16; }
        if self.fblink { rtn |= clib::UWUI_FONT_FAST_BLINK as u16; }
        if self.rev    { rtn |= clib::UWUI_FONT_REV        as u16; }
        if self.hide   { rtn |= clib::UWUI_FONT_HIDE       as u16; }
        if self.strike { rtn |= clib::UWUI_FONT_STRIKE     as u16; }

        return rtn;
    }
}

#[derive (Debug, Copy, Clone, PartialEq)]
pub struct Font
{
    attrs:       FontAttrs,
    fg:          Colour,
    bg:          Colour
}

impl Font
{
    pub fn new_default() -> Self {
        Self {
            attrs:       FontAttrs::new_default(),
            fg:          Colour::NoColour,
            bg:          Colour::NoColour
        }
    }

    pub fn to_font_s(&self) -> clib::uwui_font_s {
        clib::uwui_font_s {
            attrs: self.attrs.to_font_attr_t(),
            fg:    self.fg.to_colour_t(),
            bg:    self.bg.to_colour_t()
        }
    }
}
