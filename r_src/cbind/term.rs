//use std::ptr;
use std::mem;

use crate::pos::{Size, Pos};

use crate::chr::Chr;

use crate::err::ErrorInfo;

//use crate::cbind::font;
use crate::cbind::clib;
use crate::cbind::clib::ErrorResult;

pub struct Term
{
    ptr: *mut clib::uwui_term_s
}

impl Drop for Term
{
    fn drop(&mut self)
    {
        unsafe
        {
            if !self.ptr.is_null()
            {
                clib::uwui_term_destroy(self.ptr);
            }
        }
    }
}

impl Term
{
    pub fn new() -> Result<Self, ErrorInfo>
    {
        let mut errmsg = clib::new_errmsg();

        unsafe {
            let rtn = Self {
                ptr: clib::uwui_term_create(0, 1, errmsg.as_mut_ptr())
            };

            if rtn.ptr.is_null()
            {
                return Err(ErrorInfo::from_errmsg(errmsg));
            }

            return Ok(rtn);
        }
    }


    pub fn get_state(&self) -> clib::uwui_term_state_s
    {
        unsafe {
            let mut state: clib::uwui_term_state_s = mem::zeroed();

            clib::uwui_term_get_state(self.ptr, &mut state);

            return state;
        }

    }

    pub fn set_state(&self, state: &clib::uwui_term_state_s) -> Result<(), ErrorInfo>
    {
        let mut errmsg = clib::new_errmsg();

        unsafe {
            let res = clib::uwui_term_set_state(
                self.ptr, state, errmsg.as_mut_ptr()
            );

            if res.is_err()
            {
                return Err(ErrorInfo::from_errmsg(errmsg));
            }
        }


        return Ok(());
    }

    pub fn display(
        &self,
        pos: Pos,
        chr: Chr,
    ) -> Result<(), ErrorInfo>
    {
        let mut errmsg = clib::new_errmsg();

        unsafe
        {
            let mut string: Vec<u8> = chr.get_utf8();
            string.push(0 as u8);

            let font = chr.get_font().to_font_s();

            let res = clib::uwui_term_display(
                self.ptr,
                &string[..] as * const _ as * const i8, &font,
                pos.x as i32, pos.y as i32,
                errmsg.as_mut_ptr()
            );

            if res.is_err()
            {
                return Err(ErrorInfo::from_errmsg(errmsg));
            }
        }

        return Ok(());
    }

    pub fn clear_screen(&self) -> Result<(), ErrorInfo>
    {
        let mut errmsg = clib::new_errmsg();

        unsafe
        {
            let res = clib::uwui_term_clear_screen(self.ptr, errmsg.as_mut_ptr());

            if res.is_err()
            {
                return Err(ErrorInfo::from_errmsg(errmsg));
            }
        }

        return Ok(());
    }

    pub fn get_dims(&self) -> Size
    {
        let mut w: u32 = 0;
        let mut h: u32 = 0;

        unsafe
        {
            clib::uwui_term_get_dims(self.ptr, &mut w, &mut h);
        }

        return Size::new(w as i64, h as i64);
    }
}
