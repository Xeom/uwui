use crate::chr::Chr;
use crate::displayable::Displayable;
use crate::interactable::Interactable;
use crate::pos::{Rect, Pos};
use crate::screen::Drawable;
use std::rc::{Weak, Rc};

pub trait WidgetContents:
    Displayable + Interactable
{
}

pub struct Widget
{
    pos:      Pos,

    children: Vec<Rc<Widget>>,
    parent:   Option<Weak<Widget>>,

    pub contents: Box<dyn WidgetContents>
}

impl Drawable for Widget
{
    fn get_dims(&self) -> Rect {
        Rect::from_size(
            self.pos,
            self.contents.get_size()
        )
    }

    fn get_chr_at(&self, pos: Pos) -> Option<Chr>
    {
        if self.get_dims().contains(pos)
        {
            let relpos = Pos::sub(pos, self.get_pos());

            return match self.get_child_at(pos) {
                Some(child) => child.get_chr_at(relpos),
                None        => self.contents.get_chr_at(relpos)
            }
        }

        return None;
    }
}


impl Widget
{
    pub fn new(contents: Box<dyn WidgetContents>) -> Self
    {
        return Self {
            pos:      Pos::zero(),
            children: Vec::new(),
            parent:   None,
            contents: contents
        };
    }

    pub fn get_parent(&self) -> Option<Rc<Widget>> {
        self.parent.as_ref()?.upgrade()
    }

    pub fn iter_children(&self) -> Vec<Rc<Widget>> {
        self.children.iter().map(Rc::clone).collect()
    }

    pub fn add_child(&mut self, child: Widget)
    {
        self.children.push(Rc::new(child));
    }

    pub fn set_pos(&mut self, pos: Pos)
    {
        self.pos = pos;
    }

    fn get_child_at(&self, pos: Pos) -> Option<&Widget>
    {
        if self.get_dims().contains(pos)
        {
            let relpos = Pos::sub(pos, self.get_pos());

            for child in self.children.iter()
            {
                if child.get_dims().contains(relpos)
                {
                    return Some(child);
                }
            }
        }

        return None;
    }
}

