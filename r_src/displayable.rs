use crate::chr::Chr;
use std::convert::TryFrom;
use crate::cbind::font::Font;
use crate::pos::{Pos, Size};

pub trait Displayable
{
    fn get_chr_at(&self, _pos: Pos) -> Option<Chr> { None }
    fn get_size(&self) -> Size { Size::new(3, 3) }
}

pub trait StringDisplayable
{
    fn get_chr_font(&self, _pos: Pos) -> Font { Font::new_default() }
    fn get_lines(&self) -> Vec<&str>;

    fn get_line(&self, pos: Pos) -> Option<&str>
    {
        if let Ok(ind) = usize::try_from(pos.y)
        {
            return self.get_lines().get(ind).map(|x| *x);
        }
        else
        {
            return None;
        }
    }

    fn get_line_chrs(&self, pos: Pos) -> Option<Vec<Chr>>
    {
        if let Some(line) = self.get_line(pos)
        {
            let mut rtn = Vec::new();

            for (x, c) in line.chars().enumerate()
            {
                let font = self.get_chr_font(Pos::new(x as i64, pos.y));
                rtn.push(Chr::new(c, font));
            }

            return Some(rtn);
        }

        return None;
    }
}

impl<T> Displayable for T
    where T: StringDisplayable
{
    fn get_chr_at(&self, pos: Pos) -> Option<Chr> {
        match self.get_line_chrs(pos) {
            Some(line) => line.get(pos.x as usize).map(Clone::clone),
            None       => None
        }
    }

    fn get_size(&self) -> Size
    {
        let lines = self.get_lines();

        return Size::new(
            lines.iter().map(|l| l.len()).max().unwrap_or(0) as i64,
            lines.len() as i64
        );
    }
}
