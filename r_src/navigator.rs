use crate::interactable::InteractionState;
use crate::widget::Widget;
use crate::err::ErrorInfo;
use std::rc::Rc;

pub struct Navigator
{
    focused:    Option<Rc<Widget>>,
    selected:   Option<Rc<Widget>>
}

pub fn get_focusable_parent(widget: Rc<Widget>) -> Option<Rc<Widget>>
{
    match widget.get_parent() {
        Some(parent) => {
            if parent.contents.can_focus() {
                Some(parent)
            } else {
                get_focusable_parent(parent)
            }
        }
        None => { None }
    }
}

pub fn get_selectable_children(widget: &Widget, vec: &mut Vec<Rc<Widget>>)
{
    for child in widget.iter_children()
    {
        if child.contents.can_select()
        {
            vec.push(child)
        }
        else
        {
            get_selectable_children(&child, vec);
        }
    }
}

pub fn optional_rc_ptr_eq<T>(a: &Option<Rc<T>>, b: &Option<Rc<T>>) -> bool {
    match (a, b) {
        (Some(arc), Some(brc)) => { Rc::ptr_eq(&arc, &brc) }
        _ => { false }
    }
}

impl Navigator
{
    pub fn get_selectable(&self) -> Vec<Rc<Widget>>
    {
        let mut rtn = Vec::new();

        if let Some(selected) = &self.selected
        {
            get_selectable_children(&selected, &mut rtn);
        }

        return rtn;
    }

    fn select_nth(&mut self, n: i32)
    {
        let mut ind: i32 = 0;
        let selectable = self.get_selectable();

        if selectable.len() > 0
        {
            for (i, s) in selectable.iter().enumerate()
            {
                if optional_rc_ptr_eq(&Some(s.clone()), &self.selected)
                {
                    ind = i as i32;
                }
            }

            ind += n;
            ind %= selectable.len() as i32;
            self.selected = selectable.get(ind as usize).map(Rc::clone);
        }
        else
        {
            self.selected = None;
        }
    }

    pub fn select_next(&mut self)
    {
        self.select_nth(1);
    }

    pub fn select_prev(&mut self)
    {
        self.select_nth(-1);
    }

    pub fn get_selected(&self) -> Option<Rc<Widget>> {
        self.selected.as_ref().map(Rc::clone)
    }

    pub fn get_focused(&self) -> Option<Rc<Widget>> {
        self.focused.as_ref().map(Rc::clone)
    }

    pub fn focus(&mut self)
    {
        if let Some(selected) = self.get_selected()
        {
            if selected.contents.can_focus()
            {
                self.set_focused(selected.clone());
            }
        }
    }

    pub fn press(&mut self) -> Result<(), ErrorInfo>
    {
        if let Some(selected) = self.get_selected()
        {
            if selected.contents.can_press()
            {
                selected.contents.press()?;
            }
            else
            {
                self.focus();
            }
        }

        return Ok(());
    }

    pub fn set_focused(&mut self, focused: Rc<Widget>)
    {
        self.focused = Some(focused);
    }

    pub fn set_selected(&mut self, selected: Rc<Widget>)
    {
        self.selected = Some(selected);
    }

    pub fn get_interactable_state(&mut self, widget: Rc<Widget>)
        -> InteractionState
    {
        if optional_rc_ptr_eq(&Some(widget.clone()), &self.selected) {
            InteractionState::Selected
        } else if optional_rc_ptr_eq(&Some(widget.clone()), &self.focused) {
            InteractionState::Focused
        } else if optional_rc_ptr_eq(&get_focusable_parent(widget), &self.focused) {
            InteractionState::Selectable
        } else {
            InteractionState::Idle
        }
    }

    
}
