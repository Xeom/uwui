use crate::cbind::clib;
use std::str;

#[derive (Debug)]
pub struct ErrorInfo
{
    msg: String
}

impl ErrorInfo
{
    pub fn from_str(msg: &str) -> Self
    {
        Self { msg: String::from(msg) }
    }

    pub fn from_errmsg(errmsg: clib::uwui_errmsg_t) -> Self
    {
        let buf: Vec<u8> = errmsg
            .iter()
            .take_while(|x| **x != 0)
            .map(|x| *x as u8)
            .collect();

        return Self {
            msg: String::from_utf8(buf).unwrap_or_else(
                |_x| String::from("Invalid UTF8 Error.")
            )
        }
    }

    pub fn to_errmsg(&self, errmsg: &mut clib::uwui_errmsg_t)
    {
        let bytes = self.msg.as_bytes();

        for (dst, src) in errmsg.iter_mut().zip(bytes.iter().chain(&[0]))
        {
            *dst = *src;
        }

        errmsg[clib::UWUI_ERRMSG_LEN as usize - 1] = 0 as u8;
    }

    pub fn print(&self)
    {
        println!("My apologies, but {}\n", self.msg);
    }

}
