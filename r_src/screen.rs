use crate::chr::Chr;
use crate::cbind::clib;
use crate::pos::{Pos, Size, PosIterator, Rect};
use crate::err::ErrorInfo;
use crate::cbind::term::Term;

pub trait Drawable
{
    fn get_dims(&self) -> Rect;
    fn get_pos(&self)  -> Pos { self.get_dims().top_left() }

    fn get_chr_at(&self, pos: Pos) -> Option<Chr>;
}

pub trait Screen
{
    fn get_dims(&self) -> Size;
    fn set_chr(&mut self, pos: Pos, chr: Chr);
    fn redraw(&mut self) -> Result<(), ErrorInfo>;
    fn update(&mut self) -> Result<(), ErrorInfo>;

    fn draw(&mut self, obj: impl Drawable) -> Result<(), ErrorInfo>
    {
        for pos in self.iter_pos()
        {
            match obj.get_chr_at(pos)
            {
                Some(c) => { self.set_chr(pos, c); }
                None    => { self.set_chr(pos, Chr::new_space()); }
            }
        }

        return Ok(());
    }

    fn ind_to_pos(&self, i: usize) -> Option<Pos> {
        self.get_dims().ind_to_pos_from_origin(i)
    }

    fn pos_to_ind(&self, p: Pos) -> Option<usize> {
        self.get_dims().pos_to_ind_from_origin(p)
    }

    fn iter_pos(&self) -> PosIterator {
        self.get_dims().iter_from_origin()
    }
}

pub struct TermScreen
{
    term:  Term,
    size:  Size,
    front: Vec<Chr>,
    rear:  Vec<Chr>
}

impl Screen for TermScreen
{
    fn get_dims(&self) -> Size { self.size }

    fn set_chr(&mut self, pos: Pos, chr: Chr)
    {
        match self.pos_to_ind(pos)
        {
            Some(ind) => { self.rear[ind] = chr; }
            None      => { }
        }
    }

    fn redraw(&mut self) -> Result<(), ErrorInfo>
    {
        self.term.clear_screen()?;
        self.update()?;

        return Ok(());
    }

    fn update(&mut self) -> Result<(), ErrorInfo>
    {
        for (pos, (frontc, rearc)) in self.iter_pos().zip(
            self.front.iter_mut().zip(self.rear.iter_mut())
        )
        {
            if *frontc != *rearc
            {
                *frontc = *rearc;
                self.term.display(pos, *frontc)?;
            }
        }

        return Ok(());
    }
}

impl TermScreen
{
    pub fn new(term: Term) -> Self
    {
        let mut rtn = Self {
            term: term,
            size: Size::zero(),
            front: Vec::new(),
            rear:  Vec::new()
        };

        rtn.update_dims();

        return rtn;
    }

    fn new_blank_buffer(&mut self) -> Vec<Chr> {
        vec![Chr::new_space(); self.size.area()]
    }

    pub fn clear_screen(&mut self) -> Result<(), ErrorInfo>
    {
        self.term.clear_screen()?;
        self.front = self.new_blank_buffer();

        return Ok(());
    }

    pub fn setup_term(&mut self) -> Result<(), ErrorInfo>
    {
        let mut state = self.term.get_state();
        state.flags  |= clib::UWUI_TERM_FLAG_ALT_SCR;
        self.term.set_state(&state)?;

        return Ok(());
    }

    pub fn update_dims(&mut self)
    {
        self.size = self.term.get_dims();
        self.front = self.new_blank_buffer();
        self.rear  = self.new_blank_buffer();
    }
}
