#include <stdbool.h>
#include "private/term/font.h"
#include "private/term/term.h"
#include "private/term/io.h"

static uwui_res_t uwui_term_write_colour(
    uwui_term_s   *term,
    uwui_colour_t  col,
    bool           bg,
    uwui_errmsg_t  errmsg
)
{
    unsigned base;
    uwui_colour_t family;

    family = col & UWUI_COLOUR_FAMILY_MASK;

    switch (family)
    {
    case UWUI_COLOUR_FAMILY_NONE:
        break;
    case UWUI_COLOUR_FAMILY_16:
        if (bg)
            base = (col & UWUI_COLOUR_16_BRIGHT) ? 100 : 40;
        else
            base = (col & UWUI_COLOUR_16_BRIGHT) ? 90 : 30;

        TRY(uwui_term_writef(
            term, errmsg, ";%d",
            base + (col & UWUI_COLOUR_16_MASK)
        ));
        break;
    case UWUI_COLOUR_FAMILY_256:
        TRY(uwui_term_writef(
            term, errmsg, "%d;5;%d",
            bg ? 48 : 38,
            (col & UWUI_COLOUR_256_MASK)
        ));
        break;
    case UWUI_COLOUR_FAMILY_24BIT:
        TRY(uwui_term_writef(
            term, errmsg,
            "%d;2;%d;%d;%d",
            bg ? 48 : 38,
            (col & UWUI_COLOUR_24BIT_R_MASK) >> 0,
            (col & UWUI_COLOUR_24BIT_G_MASK) >> 8,
            (col & UWUI_COLOUR_24BIT_B_MASK) >> 16
        ));
        break;
    }

    return UWUI_OK;
}

static uwui_res_t uwui_term_write_attrs(
    uwui_term_s     *term,
    uint16_t         attrs,
    uwui_errmsg_t    errmsg
)
{
    uint16_t i;

    for (i = 1; attrs; ++i)
    {
        if (attrs & 0x01)
            TRY(uwui_term_writef(term, errmsg, "%d;", i));

        attrs >>= 1;
    }

    return UWUI_OK;
}

static uwui_res_t uwui_term_write_font(
    uwui_term_s       *term,
    const uwui_font_s *font,
    uwui_errmsg_t      errmsg
)
{
    TRY(uwui_term_writef(term, errmsg, CSI "0"));
    TRY(uwui_term_write_colour(term, font->fg, false, errmsg));
    TRY(uwui_term_write_colour(term, font->bg, true,  errmsg));
    TRY(uwui_term_write_attrs(term, font->attrs, errmsg));
    TRY(uwui_term_writef(term, errmsg, "m"));

    return UWUI_OK;
}

uwui_res_t uwui_term_set_font(
    uwui_term_s        *term,
    const uwui_font_s  *font,
    uwui_errmsg_t       errmsg
)
{
    if (memcmp(font, &(term->font), sizeof(uwui_font_s)) == 0)
        return UWUI_OK;

    TRY(uwui_term_write_font(term, font, errmsg));

    memcpy(&(term->font), font, sizeof(uwui_font_s));

    return UWUI_OK;
}

uint32_t uwui_colour_default(void)
{
    return UWUI_COLOUR_DEFAULT;
}

uint32_t uwui_colour_16(uint8_t num, bool bright)
{
    return UWUI_COLOUR_16(num, bright);
}

uint32_t uwui_colour_256(uint8_t num)
{
    return UWUI_COLOUR_256(num);
}

uint32_t uwui_colour_24bit(uint8_t r, uint8_t g, uint8_t b)
{
    return UWUI_COLOUR_24BIT(r, g, b);
}
