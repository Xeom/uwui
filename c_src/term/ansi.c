#include "private/term/term.h"

static uwui_res_t uwui_term_write_flag(
    uwui_term_s     *term,
    uint32_t         flag,
    bool             val,
    uwui_errmsg_t    errmsg
)
{
    const char *hi, *lo;

    switch (flag)
    {
    case UWUI_TERM_FLAG_SHOW_CUR:
        hi = CSI "?25h";
        lo = CSI "?25l";
        break;
    case UWUI_TERM_FLAG_PASTE_BRA:
        hi = CSI "?2004h";
        lo = CSI "?2004l";
        break;
    case UWUI_TERM_FLAG_KEYPAD:
        hi = ESC "\075";
        lo = ESC "\076";
        break;
    case UWUI_TERM_FLAG_ALT_SCR:
        hi = CSI "?1049h" CSI "2J";
        lo = CSI "?1049l";
        break;
    default:
        ERRMSG(
            errmsg,
            "Unknown flag 0x%08x.",
            flag
        );
        return UWUI_ERR;
    }

    if (val)
    {
        TRY(uwui_term_write(term, hi, strlen(hi), errmsg));
    }
    else
    {
        TRY(uwui_term_write(term, lo, strlen(lo), errmsg));
    }

    return UWUI_OK;
}

uwui_res_t uwui_term_update_flags(
    uwui_term_s     *term,
    uint32_t         new,
    uwui_errmsg_t    errmsg
)
{
    uint32_t old, changed, i;

    old = term->flags;
    term->flags = new;
    changed = old ^ new;

    for (i = 1; i; i <<= 1)
    {
        if (changed & i)
            TRY(uwui_term_write_flag(term, i, new & i, errmsg));
    }

    return UWUI_OK;
}

uwui_res_t uwui_term_move_cur(
    uwui_term_s  *term,
    int           col,
    int           row,
    uwui_errmsg_t errmsg
)
{
    if (term->currow < 0 ||
        term->curcol < 0 ||
        term->currow != row ||
        term->curcol != col)
    {
        TRY(uwui_term_writef(
            term, errmsg, CSI "%d;%dH", row + 1, col + 1
        ));
        term->currow = row;
        term->curcol = col;
    }

    return UWUI_OK;
}

uwui_res_t uwui_term_clear_screen(
    uwui_term_s   *term,
    uwui_errmsg_t  errmsg
)
{
    TRY(uwui_term_writef(term, errmsg, CSI "2J"));

    return UWUI_OK;
}

static inline size_t utf8_num_codepoints(const uint8_t *str)
{
    size_t rtn;
    rtn = 0;

    for (; *str; ++str)
    {
        if ((*str & 0xc0) != 0x80)
            ++rtn;
    }

    return rtn;
}


uwui_res_t uwui_term_write_str(
    uwui_term_s   *term,
    const char    *str,
    uwui_errmsg_t  errmsg
)
{
    TRY(uwui_term_write(term, str, strlen(str), errmsg));
    term->curcol += utf8_num_codepoints((uint8_t *)str);

    return UWUI_OK;
}
