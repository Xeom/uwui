#include <unistd.h>
#include <stdarg.h>
#include <poll.h>
#include "private/term/term.h"

uwui_res_t uwui_term_write(
    uwui_term_s  *term,
    const char   *data,
    size_t        len,
    uwui_errmsg_t errmsg
)
{
    while (len > 0)
    {
        ssize_t res;
        res = write(term->outfd, data, len);

        if (res < 0)
        {
            ERRMSG_ERRNO(
                errmsg,
                "Could not write to terminal"
            );
            return UWUI_ERR;
        }
        else if (res < (ssize_t)len)
        {
            len  -= res;
            data += res;
        }
        else
        {
            break;
        }
    }

    return UWUI_OK;
}

uwui_res_t uwui_term_writef(
    uwui_term_s  *term,
    uwui_errmsg_t errmsg,
    const char   *fmt,
    ...
)
{
    ssize_t res;
    char    junk[1];
    va_list args;

    va_start(args, fmt);
    res = vsnprintf(junk, 1, fmt, args);
    va_end(args);

    if (res < 0)
    {
        ERRMSG(
            errmsg,
            "Could not format terminal message."
        );
        return UWUI_ERR;
    }

    char buf[res + 1];

    va_start(args, fmt);
    res = vsnprintf(buf, sizeof(buf), fmt, args);
    va_end(args);

    TRY(uwui_term_write(
        term,
        buf, sizeof(buf),
        errmsg
    ));

    return UWUI_OK;
}

uwui_res_t uwui_term_read(
    uwui_term_s  *term,
    char         *data,
    size_t       *len,
    uwui_errmsg_t errmsg
)
{
    int res;

    struct pollfd fds[] = {
        { .fd = term->infd, .events = POLLIN }
    };

    res = poll(fds, DIM(fds), 0);

    if (res < 0)
    {
        ERRMSG(
            errmsg,
            "Could not poll terminal: %s",
            strerror(errno)
        );
        return UWUI_ERR;
    }
    else if (res > 0)
    {
        ssize_t nread;
        nread = read(term->infd, data, *len);

        if (nread < 0)
        {
            ERRMSG(
                errmsg,
                "Could not read from terminal: %s",
                strerror(errno)
            );
            return UWUI_ERR;
        }
        else
        {
            *len = (size_t)nread;
        }
    }
    else
    {
        *len = 0;
    }

    return UWUI_OK;
}
