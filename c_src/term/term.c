#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include "private/term/term.h"
#include "private/plat/mem.h"

static uwui_res_t uwui_term_init(
    uwui_term_s  *term,
    int           infd,
    int           outfd,
    uwui_errmsg_t errmsg
)
{
    memset(term, '\0', sizeof(uwui_term_s));

    term->flags = UWUI_TERM_FLAG_SHOW_CUR;
    term->infd  = infd;
    term->outfd = outfd;

    memcpy(&(term->font), &UWUI_DEFAULT_FONT, sizeof(term->font));

    term->currow = -1;
    term->curcol = -1;

    uwui_term_get_state(term, &(term->restorestate));

    return UWUI_OK;
}

static void uwui_term_kill(
    uwui_term_s *term
)
{
    uwui_term_set_state(term, &(term->restorestate), NULL);

    memset(term, '\0', sizeof(uwui_term_s));
}

uwui_term_s *uwui_term_create(
    int           infd,
    int           outfd,
    uwui_errmsg_t errmsg
)
{
    uwui_term_s *rtn;

    if (uwui_alloc((void **)&rtn, sizeof(uwui_term_s), errmsg) != UWUI_OK)
    {
        return NULL;
    }

    if (uwui_term_init(rtn, infd, outfd, errmsg) != UWUI_OK)
    {
        uwui_free(rtn);
        return NULL;
    }

    return rtn;
}

void uwui_term_destroy(
    uwui_term_s  *term
)
{
    if (!term)
        return;

    uwui_term_kill(term);
    uwui_free(term);
}

void uwui_term_get_state(
    const uwui_term_s *term,
    uwui_term_state_s *state
)
{
    memset(state, '\0', sizeof(uwui_term_state_s));

    state->flags = term->flags;
    tcgetattr(term->outfd, &(state->attrs));
}

uwui_res_t uwui_term_set_state(
    uwui_term_s             *term,
    const uwui_term_state_s *state,
    uwui_errmsg_t            errmsg
)
{
    tcsetattr(term->outfd, TCSANOW, &(state->attrs));

    TRY(uwui_term_update_flags(term, state->flags, errmsg));
    TRY(uwui_term_set_font(term, &UWUI_DEFAULT_FONT, errmsg));

    term->currow = -1;
    term->curcol = -1;

    return UWUI_OK;
}

void uwui_term_get_dims(
    uwui_term_s    *term,
    unsigned       *w,
    unsigned       *h
)
{
    struct winsize wsz;

    if (ioctl(term->infd, TIOCGWINSZ, &wsz) < 0)
    {
        *w = 80;
        *h = 30;
    }
    else
    {
        *w = wsz.ws_col;
        *h = wsz.ws_row;
    }
}

uwui_res_t uwui_term_display(
    uwui_term_s        *term,
    const char         *str,
    const uwui_font_s  *font,
    int                 col,
    int                 row,
    uwui_errmsg_t       errmsg
)
{
    TRY(uwui_term_move_cur(term, col, row, errmsg));
    TRY(uwui_term_set_font(term, font, errmsg));
    TRY(uwui_term_write_str(term, str, errmsg));

    return UWUI_OK;
}


