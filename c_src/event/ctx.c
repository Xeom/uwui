#include "private/event/ctx.h"
#include "private/event/consumer.h"
#include "private/event/event.h"
#include "private/plat/mem.h"

static uwui_res_t uwui_event_ctx_pop_event(
    uwui_event_ctx_s *ctx,
    uwui_event_s    **event,
    uwui_errmsg_t     errmsg
);

static uwui_res_t uwui_event_ctx_init(
    uwui_event_ctx_s *ctx,
    uwui_errmsg_t     errmsg
)
{
    TRY(uwui_table_init(&(ctx->consumers), 256, errmsg));
    uwui_list_init(&(ctx->queue));

    return UWUI_OK;
}

static void uwui_event_ctx_kill(
    uwui_event_ctx_s *ctx
)
{
    uwui_list_s *list;

    while (uwui_table_iterator_next(
        &(ctx->consumers), &UWUI_TABLE_ITERATOR,
        NULL, NULL, (void **)&list, NULL
    ))
    {
        uwui_event_consumer_s *consumer;
        while ((consumer = uwui_list_get_event_consumer(list, NULL, +1)))
        {
            uwui_event_consumer_destroy(consumer);
        }
    }

    uwui_table_kill(&(ctx->consumers));

    uwui_event_s *event;

    while (uwui_list_len(&(ctx->queue)))
    {
        uwui_event_ctx_pop_event(ctx, &event, NULL);
        uwui_event_destroy(event);
    }
}

uwui_event_ctx_s *uwui_event_ctx_create(
    uwui_errmsg_t errmsg
)
{
    uwui_event_ctx_s *rtn;

    if (uwui_alloc((void **)&rtn, sizeof(uwui_event_ctx_s), errmsg) != UWUI_OK)
    {
        return NULL;
    }

    if (uwui_event_ctx_init(rtn, errmsg) != UWUI_OK)
    {
        uwui_free(rtn);
        return NULL;
    }

    return rtn;
}

void uwui_event_ctx_destroy(
    uwui_event_ctx_s *ctx
)
{
    if (ctx)
    {
        uwui_event_ctx_kill(ctx);
        uwui_free(ctx);
    }
}


static uwui_res_t uwui_event_ctx_rem_consumer_list(
    uwui_event_ctx_s        *ctx,
    const uwui_event_spec_s *dirtyspec,
    uwui_errmsg_t            errmsg
)
{
    uwui_event_spec_s spec = uwui_event_spec_sanitize(dirtyspec);

    if (uwui_table_contains(&(ctx->consumers), &spec, sizeof(spec)))
    {
        TRY(uwui_table_del(
            &(ctx->consumers),
            &spec, sizeof(spec),
            errmsg
        ));
    }

    return UWUI_OK;
}

static uwui_res_t uwui_event_ctx_add_consumer_list(
    uwui_event_ctx_s        *ctx,
    const uwui_event_spec_s *dirtyspec,
    uwui_errmsg_t            errmsg
)
{
    uwui_event_spec_s spec = uwui_event_spec_sanitize(dirtyspec);

    if (!uwui_table_contains(&(ctx->consumers), &spec, sizeof(spec)))
    {
        uwui_list_s new;
        uwui_list_init(&new);

        TRY(uwui_table_set(
            &(ctx->consumers),
            &spec, sizeof(spec),
            &new,  sizeof(new),
            errmsg
        ));
    }

    return UWUI_OK;
}

static uwui_list_s *uwui_event_ctx_get_consumers(
    uwui_event_ctx_s        *ctx,
    const uwui_event_spec_s *dirtyspec
)
{
    uwui_event_spec_s spec = uwui_event_spec_sanitize(dirtyspec);

    uwui_list_s *rtn;

    uwui_table_get(
        &(ctx->consumers),
        &spec, sizeof(spec),
        (void **)&rtn, NULL
    );

    return rtn;
}

uwui_res_t uwui_event_ctx_push_event(
    uwui_event_ctx_s *ctx,
    uwui_event_s     *event,
    uwui_errmsg_t     errmsg
)
{
    TRY(uwui_list_ins_event(&(ctx->queue), NULL, event, errmsg));

    return UWUI_OK;
}

static uwui_res_t uwui_event_ctx_pop_event(
    uwui_event_ctx_s *ctx,
    uwui_event_s    **rtn,
    uwui_errmsg_t     errmsg
)
{
    uwui_event_s *event;

    event = uwui_list_get_event(&(ctx->queue), NULL, +1);
    TRY(uwui_list_pop_event(&(ctx->queue), event, errmsg));

    *rtn = event;

    return UWUI_OK;
}

static uwui_res_t uwui_event_ctx_dispatch_event_to_spec(
    uwui_event_ctx_s  *ctx,
    uwui_event_s      *event,
    uwui_event_spec_s *spec,
    uwui_errmsg_t      errmsg
)
{
    uwui_list_s *list;

    list = uwui_event_ctx_get_consumers(ctx, spec);

    if (list)
    {
        uwui_event_consumer_s *consumer;

        consumer = NULL;
        while ((consumer = uwui_list_get_event_consumer(list, consumer, +1)))
        {
            TRY(uwui_event_consumer_consume(consumer, event, errmsg));
        }
    }

    return UWUI_OK;
}

static uwui_res_t uwui_event_ctx_dispatch_event(
    uwui_event_ctx_s *ctx,
    uwui_event_s     *event,
    uwui_errmsg_t     errmsg
)
{
    TRY(uwui_event_ctx_dispatch_event_to_spec(
        ctx, event,
        &(uwui_event_spec_s){ event->producer, event->type },
        errmsg
    ));

    if (event->producer != NULL)
    {
        TRY(uwui_event_ctx_dispatch_event_to_spec(
            ctx, event,
            &(uwui_event_spec_s){ NULL, event->type },
            errmsg
        ));
    }

    if (event->type != UWUI_EVENT_ANY)
    {
        TRY(uwui_event_ctx_dispatch_event_to_spec(
            ctx, event,
            &(uwui_event_spec_s){ event->producer, UWUI_EVENT_ANY },
            errmsg
        ));
    }

    return UWUI_OK;
}

uwui_res_t uwui_event_ctx_dispatch_next(
    uwui_event_ctx_s *ctx,
    uwui_errmsg_t     errmsg
)
{
    uwui_event_s *event;

    TRY(uwui_event_ctx_pop_event(ctx, &event, errmsg));
    TRY(uwui_event_ctx_dispatch_event(ctx, event, errmsg));
    uwui_event_destroy(event);

    return UWUI_OK;
}

uwui_res_t uwui_event_ctx_add_consumer(
    uwui_event_ctx_s        *ctx,
    uwui_event_consumer_s   *consumer,
    uwui_errmsg_t            errmsg
)
{
    uwui_list_s *list;

    TRY(uwui_event_ctx_add_consumer_list(ctx, &(consumer->spec), errmsg));
    list = uwui_event_ctx_get_consumers(ctx, &(consumer->spec));
    TRY(uwui_list_ins_event_consumer(list, NULL, consumer, errmsg));

    return UWUI_OK;
}

uwui_res_t uwui_event_ctx_rem_consumer(
    uwui_event_ctx_s        *ctx,
    uwui_event_consumer_s   *consumer,
    uwui_errmsg_t            errmsg
)
{
    uwui_list_s *list;

    list = uwui_event_ctx_get_consumers(ctx, &(consumer->spec));

    if (list)
    {
        TRY(uwui_list_pop_event_consumer(list, consumer, errmsg));

        if (uwui_list_len(list) == 0)
        {
            TRY(uwui_event_ctx_rem_consumer_list(
                ctx,
                &(consumer->spec),
                errmsg
            ));
        }
    }

    return UWUI_OK;
}
