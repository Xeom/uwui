#include "private/event/event.h"
#include "private/event/producer.h"
#include "private/plat/mem.h"

uwui_event_spec_s uwui_event_spec_sanitize(
    const uwui_event_spec_s *spec
)
{
    uwui_event_spec_s rtn;
    memset(&rtn, '\0', sizeof(uwui_event_spec_s));
    rtn.producer = spec->producer;
    rtn.typ      = spec->typ;

    return rtn;
}


uwui_res_t uwui_event_init(
    uwui_event_s          *event,
    uwui_event_producer_s *producer,
    uwui_event_type_t      type,
    const uint8_t         *data,
    size_t                 len,
    uwui_errmsg_t          errmsg
)
{
    event->ctx      = producer->ctx;
    event->producer = producer;
    event->type     = type;

    uwui_vec_init(&(event->data), sizeof(uint8_t));
    TRY(uwui_vec_add(&(event->data), len, data, errmsg));

    return UWUI_OK;
}

void uwui_event_kill(
    uwui_event_s *event
)
{
    uwui_vec_kill(&(event->data));
}

uwui_event_s *uwui_event_create(
    uwui_event_producer_s *producer,
    uwui_event_type_t      type,
    const uint8_t         *data,
    size_t                 len,
    uwui_errmsg_t          errmsg
)
{
    uwui_event_s *rtn;

    if (uwui_alloc((void **)&rtn, sizeof(uwui_event_s), errmsg) != UWUI_OK)
    {
        return NULL;
    }

    if (uwui_event_init(rtn, producer, type, data, len, errmsg) != UWUI_OK)
    {
        uwui_free(rtn);
        return NULL;
    }

    return rtn;
}

void uwui_event_destroy(
    uwui_event_s *event
)
{
    if (event)
    {
        uwui_event_kill(event);
        uwui_free(event);
    }
}

const uint8_t *uwui_event_get_data(
    const uwui_event_s *event
)
{
    return uwui_vec_get_const(&(event->data), 0);
}

size_t uwui_event_get_data_len(
    const uwui_event_s *event
)
{
    return uwui_vec_len(&(event->data));
}

uwui_event_type_t uwui_event_get_type(
    const uwui_event_s *event
)
{
    return event->type;
}
