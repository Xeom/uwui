#include "private/event/producer.h"
#include "private/event/event.h"
#include "private/event/ctx.h"
#include "private/plat/mem.h"

uwui_res_t uwui_event_producer_init(
    uwui_event_producer_s  *producer,
    uwui_event_ctx_s       *ctx,
    uwui_errmsg_t           errmsg
)
{
    producer->ctx = ctx;

    return UWUI_OK;
}

void uwui_event_producer_kill(
    uwui_event_producer_s  *producer
)
{
}

uwui_event_producer_s *uwui_event_producer_create(
    uwui_event_ctx_s *ctx,
    uwui_errmsg_t     errmsg
)
{
    uwui_event_producer_s *rtn;

    /* Allocate a producer */
    if (uwui_alloc(
        (void **)&rtn, sizeof(uwui_event_producer_s), errmsg
    ) != UWUI_OK)
    {
        return NULL;
    }

    if (uwui_event_producer_init(rtn, ctx, errmsg) != UWUI_OK)
    {
        uwui_free(rtn);
        return NULL;
    }

    return rtn;
}

void uwui_event_producer_destroy(
    uwui_event_producer_s *producer
)
{
    if (producer)
    {
        uwui_event_producer_kill(producer);
        uwui_free(producer);
    }
}

uwui_res_t uwui_event_push(
    uwui_event_producer_s *producer,
    uwui_event_type_t      type,
    const uint8_t         *data,
    size_t                 len,
    uwui_errmsg_t          errmsg
)
{
    uwui_event_s *event;

    event = uwui_event_create(producer, type, data, len, errmsg);

    if (!event)
        return UWUI_ERR;

    TRY(uwui_event_ctx_push_event(producer->ctx, event, errmsg));

    return UWUI_OK;
}
