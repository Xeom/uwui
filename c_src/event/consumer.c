#include "private/event/consumer.h"
#include "private/event/event.h"
#include "private/event/ctx.h"
#include "private/plat/mem.h"
#include "private/common.h"

uwui_res_t uwui_event_consumer_init(
    uwui_event_consumer_s   *consumer,
    uwui_event_ctx_s        *ctx,
    const uwui_event_spec_s *spec,
    uwui_errmsg_t            errmsg
)
{
    uwui_event_spec_s cleanspec = uwui_event_spec_sanitize(spec);

    memcpy(&(consumer->spec), &cleanspec, sizeof(cleanspec));
    consumer->ctx  = ctx;
    consumer->user = NULL;
    consumer->cb   = NULL;

    TRY(uwui_event_ctx_add_consumer(ctx, consumer, errmsg));

    return UWUI_OK;
}

void uwui_event_consumer_kill(
    uwui_event_consumer_s *consumer
)
{
    uwui_event_ctx_rem_consumer(consumer->ctx, consumer, NULL);
}

uwui_event_consumer_s *uwui_event_consumer_create(
    uwui_event_ctx_s        *ctx,
    const uwui_event_spec_s *spec,
    uwui_errmsg_t            errmsg
)
{
    uwui_event_consumer_s *rtn;

    /* Allocate a consumer */
    if (uwui_alloc(
        (void **)&rtn, sizeof(uwui_event_consumer_s), errmsg
    ) != UWUI_OK)
    {
        return NULL;
    }

    if (uwui_event_consumer_init(rtn, ctx, spec, errmsg) != UWUI_OK)
    {
        uwui_free(rtn);
        return NULL;
    }

    return rtn;
}

void uwui_event_consumer_destroy(
    uwui_event_consumer_s *consumer
)
{
    if (consumer)
    {
        uwui_event_consumer_kill(consumer);
        uwui_free(consumer);
    }
}

uwui_res_t uwui_event_consumer_consume(
    uwui_event_consumer_s *consumer,
    uwui_event_s          *event,
    uwui_errmsg_t          errmsg
)
{
    UWUI_ERRMSG_DECLARE(cberrmsg);
    if (consumer->cb(consumer->user, event, cberrmsg) != UWUI_OK)
    {
        fprintf(stderr, "Error handling callback: %s", cberrmsg);
    }
    return UWUI_OK;
}

uwui_res_t uwui_event_consumer_set_cb(
    uwui_event_consumer_s *consumer,
    void                  *user,
    uwui_res_t           (*cb)(void *, const uwui_event_s *, uwui_errmsg_t),
    uwui_errmsg_t          errmsg
)
{
    consumer->cb = cb;
    consumer->user = user;

    return UWUI_OK;
}
