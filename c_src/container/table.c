#include <stdint.h>
#include "private/container/table.h"
#include "private/container/bucket.h"

/* Magic from code I wrote in 2014 or something. Who knows what it does. */
static uint64_t uwui_table_hash(const char *mem, size_t n)
{
    uint64_t hash;
    uint64_t data;

    hash = 0x0123456789abcdef;

    while (n > 0)
    {
        if (n >= sizeof(uint64_t))
        {
            data  = *(uint64_t *)mem;
            n    -= sizeof(uint64_t);
            data += sizeof(uint64_t);
        }
        else if (n >= sizeof(uint32_t))
        {
            data  = *(uint32_t *)mem;
            data |= data << sizeof(uint32_t) * 8;

            n    -= sizeof(uint32_t);
            data += sizeof(uint32_t);
        }
        else
        {
            int i;
            data = 0;
            for (i = 0; i < (int)sizeof(uint64_t); ++i)
            {
                data <<= 8;
                data |= mem[i % n];
            }
            n = 0;
        }

        data ^= hash;

        double den1,  den2;
        double recp1, recp2;
        uint64_t mant1, mant2;

        den1 = (double)(data         & 0xffffffff);
        den2 = (double)((data >> 32) & 0xffffffff);

        den1 = (den1 * 2) - 1;
        den2 = (den2 * 2) - 1;

        recp1 = 1/den1;
        recp2 = 1/den2;

        memcpy(&mant1, &recp1, sizeof(uint64_t));
        memcpy(&mant2, &recp2, sizeof(uint64_t));

        mant1 &= (1l << 52) - 1;
        mant2 &= (1l << 52) - 1;

        hash ^=  mant1 << (32);
        hash ^= (mant2 >> (52 - 32)) & 0xffffffff;
        hash ^= data;
    }

    return hash;
}

uwui_res_t uwui_table_init(
    uwui_table_s *table,
    size_t        nbuckets,
    uwui_errmsg_t errmsg
)
{
    memset(table, '\0', sizeof(uwui_table_s));

    uwui_vec_init(&(table->buckets), sizeof(uwui_bucket_s));
    TRY(uwui_vec_ins(&(table->buckets), 0, nbuckets, NULL, errmsg));

    VEC_FOREACH(&(table->buckets), uwui_bucket_s *, bucket)
    {
        uwui_bucket_init(bucket);
    }

    return UWUI_OK;
}

void uwui_table_kill(
    uwui_table_s *table
)
{
    VEC_FOREACH(&(table->buckets), uwui_bucket_s *, bucket)
    {
        uwui_bucket_kill(bucket);
    }

    uwui_vec_kill(&(table->buckets));
}


size_t uwui_table_len(
    uwui_table_s *table
)
{
    return table->len;
}

size_t uwui_table_nbuckets(
    uwui_table_s *table
)
{
    return uwui_vec_len(&(table->buckets));
}

uwui_res_t uwui_table_resize(
    uwui_table_s *table,
    size_t        nbuckets,
    uwui_errmsg_t errmsg
)
{
    uwui_table_s orig;
    if (nbuckets != uwui_table_nbuckets(table))
    {
        memcpy(&orig, table, sizeof(uwui_table_s));
        TRY(uwui_table_init(table, nbuckets, errmsg));
    }

    uwui_table_iterator_s iter = UWUI_TABLE_ITERATOR;

    void  *key;
    size_t keylen;
    void  *val;
    size_t vallen;

    while (uwui_table_iterator_next(
        &orig, &iter,
        &key,  &keylen,
        &val,  &vallen
    ))
    {
        TRY(uwui_table_set(table, key, keylen, val, vallen, errmsg));
    }

    return UWUI_OK;
}

uwui_bucket_s *uwui_table_find_bucket(
    uwui_table_s *table,
    const void   *key,
    size_t        keylen
)
{
    uint64_t hash;
    size_t   ind;

    hash = uwui_table_hash(key, keylen);
    ind  = hash % uwui_table_nbuckets(table);

    return uwui_vec_get(&(table->buckets), ind);
}

uwui_res_t uwui_table_del(
    uwui_table_s *table,
    const void   *key,
    size_t        keylen,
    uwui_errmsg_t errmsg
)
{
    uwui_bucket_s *bucket;
    bucket = uwui_table_find_bucket(table, key, keylen);

    if (bucket)
        TRY(uwui_bucket_del(bucket, key, keylen, errmsg));

    return UWUI_OK;
}

bool uwui_table_contains(
    uwui_table_s *table,
    const void   *key,
    size_t        keylen
)
{
    void *val;
    uwui_bucket_s *bucket;
    bucket = uwui_table_find_bucket(table, key, keylen);

    if (bucket)
    {
        uwui_bucket_get(bucket, key, keylen, &val, NULL);

        if (val)
            return true;
    }

    return false;
}

uwui_res_t uwui_table_set(
    uwui_table_s *table,
    const void   *key,
    size_t        keylen,
    const void   *val,
    size_t        vallen,
    uwui_errmsg_t errmsg
)
{
    void *prev;
    uwui_bucket_s *bucket;
    bucket = uwui_table_find_bucket(table, key, keylen);

    if (bucket)
    {
        /* Delete the item if it is already present! */
        uwui_bucket_get(bucket, key, keylen, &prev, NULL);

        if (prev)
            TRY(uwui_bucket_del(bucket, key, keylen, errmsg));

        TRY(uwui_bucket_insert(bucket, key, keylen, val, vallen, errmsg));
    }

    return UWUI_OK;
}

void uwui_table_get(
    uwui_table_s *table,
    const void   *key,
    size_t        keylen,
    void        **val,
    size_t       *vallen
)
{
    uwui_bucket_s *bucket;
    bucket = uwui_table_find_bucket(table, key, keylen);

    uwui_bucket_get(bucket, key, keylen, val, vallen);
}

bool uwui_table_iterator_next(
    uwui_table_s          *table,
    uwui_table_iterator_s *iterator,
    void                 **key,
    size_t                *keylen,
    void                 **val,
    size_t                *vallen
)
{
    uwui_bucket_s *bucket;
start:

    /* Get the current bucket */
    bucket = uwui_vec_get(&(table->buckets), iterator->bucketind);

    /* If we've not run out of buckets to look in */
    if (bucket)
    {
        if (uwui_bucket_iterator_next(
            bucket, iterator,
            key,    keylen,
            val,    vallen
        ))
        {
            /* If we find an item in this bucket, we're done. */
            return true;
        }
        else
        {
            /* If we find no item in this bucket, look in the next one */
            iterator->bucketind += 1;
            goto start;
        }
    }
    else
    {
        /* We're out of buckets to look in, iteration is done. */
        return false;
    }
}
