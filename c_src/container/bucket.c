#include "private/container/bucket.h"
#include "private/container/table.h"
#include "private/plat/mem.h"

struct uwui_bucket_item
{
    uwui_list_item_s listitem;

    /* TODO: Make these nicer and more efficient */
    void       *key;
    size_t      keylen;
    void       *val;
    size_t      vallen;
};

UWUI_DECLARE_LIST_METHODS(uwui_bucket_item_s, bucket_item, listitem)


uwui_res_t uwui_bucket_item_create(
    uwui_bucket_item_s **item,
    const void          *key,
    size_t               keylen,
    const void          *val,
    size_t               vallen,
    uwui_errmsg_t        errmsg
)
{
    TRY(uwui_alloc((void **)item, sizeof(uwui_bucket_item_s), errmsg));
    memset(*item, '\0', sizeof(uwui_bucket_item_s));

    void *keybuf;
    void *valbuf;

    TRY(uwui_alloc(&keybuf, keylen, errmsg));
    TRY(uwui_alloc(&valbuf, vallen, errmsg));

    memcpy(keybuf, key, keylen);
    memcpy(valbuf, val, vallen);

    (*item)->key = keybuf;
    (*item)->val = valbuf;

    (*item)->keylen = keylen;
    (*item)->vallen = vallen;

    return UWUI_OK;
}

void uwui_bucket_item_destroy(
    uwui_bucket_item_s *item
)
{
    if (item)
    {
        if (item->key) uwui_free(item->key);
        if (item->val) uwui_free(item->val);

        uwui_free(item);
    }
}

static void uwui_bucket_item_examine(
    uwui_bucket_item_s *item,
    void              **key,
    size_t             *keylen,
    void              **val,
    size_t             *vallen
)
{
    if (key) *key = item->key;
    if (val) *val = item->val;

    if (keylen) *keylen = item->keylen;
    if (vallen) *vallen = item->vallen;
}

bool uwui_bucket_item_matches(
    uwui_bucket_item_s *item,
    const void         *key,
    size_t              keylen
)
{
    size_t  cmpkeylen;
    void   *cmpkey;

    uwui_bucket_item_examine(item, &cmpkey, &cmpkeylen, NULL, NULL);

    return keylen == cmpkeylen && memcmp(key, cmpkey, keylen) == 0;
}


void uwui_bucket_init(
    uwui_bucket_s *bucket
)
{
    uwui_list_init(&(bucket->items));
}

void uwui_bucket_kill(
    uwui_bucket_s *bucket
)
{
    uwui_bucket_item_s *item;
    while ((item = uwui_list_first_bucket_item(&(bucket->items))))
    {
        uwui_list_pop_bucket_item(&(bucket->items), item, NULL);
        uwui_bucket_item_destroy(item);
    }
}

uwui_res_t uwui_bucket_insert(
    uwui_bucket_s *bucket,
    const void    *key,
    size_t         keylen,
    const void    *val,
    size_t         vallen,
    uwui_errmsg_t  errmsg
)
{
    uwui_bucket_item_s *item;

    TRY(uwui_bucket_item_create(&item, key, keylen, val, vallen, errmsg));
    TRY(uwui_list_ins_bucket_item(&(bucket->items), NULL, item, errmsg));

    return UWUI_OK;
}

uwui_bucket_item_s *uwui_bucket_find(
    uwui_bucket_s *bucket,
    const void    *key,
    size_t         keylen
)
{
    uwui_bucket_item_s *item;

    item = NULL;
    while ((item = uwui_list_get_bucket_item(&(bucket->items), item, +1)))
    {
        if (uwui_bucket_item_matches(item, key, keylen))
            return item;
    }

    return NULL;
}

uwui_res_t uwui_bucket_del(
    uwui_bucket_s *bucket,
    const void    *key,
    size_t         keylen,
    uwui_errmsg_t  errmsg
)
{
    uwui_bucket_item_s *item;

    item = uwui_bucket_find(bucket, key, keylen);

    if (item)
    {
        TRY(uwui_list_pop_bucket_item(&(bucket->items), item, errmsg));
        uwui_bucket_item_destroy(item);
    }
    else
    {
        ERRMSG(errmsg, "Could not delete from table - key not present.");
        return UWUI_ERR;
    }

    return UWUI_OK;
}

void uwui_bucket_get(
    uwui_bucket_s *bucket,
    const void    *key,
    size_t         keylen,
    void         **val,
    size_t        *vallen
)
{
    uwui_bucket_item_s *item;

    item = uwui_bucket_find(bucket, key, keylen);

    if (item)
    {
        uwui_bucket_item_examine(item, NULL, NULL, val, vallen);
    }
    else
    {
        if (val)     *val    = NULL;
        if (vallen)  *vallen = 0;
    }
}

bool uwui_bucket_iterator_next(
    uwui_bucket_s         *bucket,
    uwui_table_iterator_s *iterator,
    void                 **key,
    size_t                *keylen,
    void                 **val,
    size_t                *vallen
)
{
    uwui_bucket_item_s *item;

    item = iterator->item;
    item = uwui_list_get_bucket_item(&(bucket->items), item, +1);
    iterator->item = item;

    if (item)
    {
        uwui_bucket_item_examine(item, key, keylen, val, vallen);

        return true;
    }
    else
    {
        return false;
    }
}

