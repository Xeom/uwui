#include "private/container/vec.h"
#include <stdlib.h>
#include <stdbool.h>

static inline bool uwui_vec_should_shrink(uwui_vec_s *vec, size_t newuse)
{
    return vec->capacity > (newuse << 2);
}

static inline bool uwui_vec_should_grow(uwui_vec_s *vec, size_t newuse)
{
    return vec->capacity < newuse;
}

static uwui_res_t uwui_vec_adjust_size(uwui_vec_s *vec, size_t newuse, uwui_errmsg_t errmsg)
{
    size_t oldcap;
    oldcap = vec->capacity;

    while (uwui_vec_should_grow(vec, newuse))
    {
        if (vec->capacity == 0)
            vec->capacity = 8;
        else
            vec->capacity *= 2;
    }

    while (uwui_vec_should_shrink(vec, newuse))
        vec->capacity /= 2;

    if (oldcap == vec->capacity)
        return UWUI_OK;

    if (vec->capacity)
    {
        vec->data = realloc(vec->data, vec->width * vec->capacity);
        if (!vec->data)
        {
            vec->capacity = 0;
            vec->used     = 0;
            ERRMSG(errmsg, "Could not allocate memory for vector.");
            return UWUI_ERR;
        }
    }
    else if (vec->data)
    {
        free(vec->data);
        vec->data = NULL;
    }

    vec->used = newuse;

    return UWUI_OK;
}

void uwui_vec_init(uwui_vec_s *vec, size_t width)
{
    memset(vec, '\0', sizeof(uwui_vec_s));
    vec->width = width;
}

void uwui_vec_kill(uwui_vec_s *vec)
{
    vec->capacity = 0;
    vec->used     = 0;
    if (vec->data)
    {
        free(vec->data);
        vec->data = NULL;
    }
}

static inline void *vec_get(uwui_vec_s *vec, size_t ind)
{
    if (ind >= vec->used)
        return NULL;

    return (void *)&(vec->data[ind * vec->width]);
}

static inline const void *vec_get_const(const uwui_vec_s *vec, size_t ind)
{
    if (ind >= vec->used)
        return NULL;

    return (const void *)&(vec->data[ind * vec->width]);
}

uwui_res_t uwui_vec_ins(
    uwui_vec_s   *vec,
    size_t        ind,
    size_t        n,
    const void   *data,
    uwui_errmsg_t errmsg
)
{
    if (n == 0)
        return UWUI_OK;

    if (ind > vec->used)
    {
        ERRMSG(errmsg, "Invalid index %lu (%lu items)", ind, vec->used);
        return UWUI_ERR;
    }

    size_t tomove;

    tomove = vec->used - ind;

    TRY(uwui_vec_adjust_size(vec, vec->used + n, errmsg));

    if (tomove > 0)
        memmove(vec_get(vec, ind + n), vec_get(vec, ind), vec->width * tomove);

    if (data)
        memcpy(vec_get(vec, ind), data, n * vec->width);
    else
        memset(vec_get(vec, ind), '\0', n * vec->width);

    return UWUI_OK;
}

uwui_res_t uwui_vec_del(
    uwui_vec_s   *vec,
    size_t        ind,
    size_t        n,
    uwui_errmsg_t errmsg
)
{
    if (n == 0)
        return UWUI_OK;

    if (ind + n > vec->used)
    {
        ERRMSG(
            errmsg,
            "Deleting too much %lu@[%lu] (%lu items)",
            n, ind, vec->used
        );
        return UWUI_ERR;
    }

    memmove(vec_get(vec, ind), vec_get(vec, ind + n), n * vec->width);

    TRY(uwui_vec_adjust_size(vec, vec->used - n, errmsg));

    return UWUI_OK;
}

const void *uwui_vec_get_const(const uwui_vec_s *vec, size_t ind)
{
    return vec_get_const(vec, ind);
}

void *uwui_vec_get(uwui_vec_s *vec, size_t ind)
{
    return vec_get(vec, ind);
}

size_t uwui_vec_len(const uwui_vec_s *vec)
{
    return vec->used;
}
