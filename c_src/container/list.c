#include "private/container/list.h"

void uwui_list_init(uwui_list_s *list)
{
    memset(list, '\0', sizeof(uwui_list_s));
}

uwui_res_t uwui_list_range_len(uwui_list_range_s range, size_t *len, uwui_errmsg_t errmsg)
{
    uwui_list_item_s *item;

    if (!range.first || !range.last)
    {
        *len = 0;
        return UWUI_OK;
    }

    item = range.first;
    *len = 1;

    while (item)
    {
        if (item == range.last)
        {
            return UWUI_OK;
        }

        *len += 1;
        item  = item->next;

        if (item == range.first)
        {
            ERRMSG(errmsg, "Circular list range.");
            return UWUI_ERR;
        }
    }

    ERRMSG(errmsg, "Incomplete list range.");
    return UWUI_ERR;
}

uwui_res_t uwui_list_ins(
    uwui_list_s       *list,
    uwui_list_item_s  *prev,
    uwui_list_range_s  toins,
    uwui_errmsg_t      errmsg
)
{
    size_t len;
    uwui_list_item_s *next;
    TRY(uwui_list_range_len(toins, &len, errmsg));

    if (len == 0)
        return UWUI_OK;

    if (prev)
        next = prev->next;
    else
        next = uwui_list_first(list);

    if (prev)
        prev->next       = toins.first;
    else
        list->range.first = toins.first;

    if (next)
        next->prev      = toins.last;
    else
        list->range.last = toins.last;

    toins.first->prev = prev;
    toins.last->next  = next;
    list->len += len;

    return UWUI_OK;
}

uwui_res_t uwui_list_pop(
    uwui_list_s       *list,
    uwui_list_range_s  tocut,
    uwui_errmsg_t      errmsg
)
{
    size_t len;
    uwui_list_item_s *next, *prev;
    TRY(uwui_list_range_len(tocut, &len, errmsg));

    if (len == 0)
        return UWUI_OK;

    prev = tocut.first->prev;
    next = tocut.last->next;

    if (prev)
        prev->next = next;
    else
        list->range.first = next;

    if (next)
        next->prev = prev;
    else
        list->range.last = prev;

    list->len -= len;

    return UWUI_OK;
}

uwui_list_item_s *uwui_list_get(
    uwui_list_s      *list,
    uwui_list_item_s *item,
    long              rel
)
{
    for (; rel > 0; --rel)
    {
        if (!item)
            item = list->range.first;
        else
            item = item->next;
    }

    for (; rel < 0; ++rel)
    {
        if (!item)
            item = list->range.last;
        else
            item = item->prev;
    }

    return item;
}

uwui_list_item_s *uwui_list_first(uwui_list_s *list)
{
    return list->range.first;
}

uwui_list_item_s *uwui_list_last(uwui_list_s *list)
{
    return list->range.last;
}

size_t uwui_list_len(uwui_list_s *list)
{
    return list->len;
}
