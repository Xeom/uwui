#include <stdlib.h>
#include "private/common.h"

uwui_res_t uwui_alloc(void **res, size_t len, uwui_errmsg_t errmsg)
{
    void *mem;
    mem = malloc(len);

    if (!mem)
    {
        ERRMSG(errmsg, "Could not allocate block of %lu Bytes.", len);
        return UWUI_ERR;
    }

    *res = mem;

    return UWUI_OK;
}

void uwui_free(void *mem)
{
    free(mem);
}
