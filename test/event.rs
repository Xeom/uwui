use std::thread;
use lib::cbind::event::{EventCtx, EventConsumer, EventProducer, EventSpec};
use lib::err::ErrorInfo;
use std::{str, time};

fn main() -> Result<(),ErrorInfo>
{
    let mut ctx: EventCtx = EventCtx::new()?;
    let mut producer = EventProducer::new(&mut ctx)?;
    let mut consumer = EventConsumer::new(&mut ctx, &EventSpec::new(None, Some(&producer)))?;

    consumer.set_cb(Box::new(|e| {println!("{}", str::from_utf8(e.get_data()).unwrap_or("OH NO")); return Ok(()); }))?;
    producer.push(0, &"MEOW I LIVE IN AN EVENT".as_bytes())?;
    ctx.dispatch_next()?;

    thread::sleep(time::Duration::from_secs(1));

    return Ok(());
}
