use std::{time, thread};
use lib::widget::{Widget, WidgetContents};
use lib::err::ErrorInfo;
use lib::cbind::term::Term;
use lib::displayable::{StringDisplayable};
use lib::interactable::Interactable;
use lib::screen::{TermScreen, Screen};

struct MyButton
{
    strings: Vec<String>
}

impl MyButton
{
    fn new(strs: &[&str]) -> Self {
        Self { strings: strs.iter().map(|s| String::from(*s)).collect() }
    }
}

impl StringDisplayable for MyButton
{
    fn get_lines(&self) -> Vec<&str> {
        self.strings.iter().map(|s| s.as_str()).collect()
    }
}

impl Interactable for MyButton
{
    fn can_press(&self)  -> bool { true }
    fn can_focus(&self)  -> bool { false }
    fn can_cursor(&self) -> bool { false }
}

impl WidgetContents for MyButton {}

fn main() -> Result<(),ErrorInfo>
{
    let button = MyButton::new(&["Hello", "World"]);
    let widget = Widget::new(Box::new(button));
    let mut screen = TermScreen::new(Term::new()?);

    screen.setup_term()?;
    screen.draw(widget)?;
    screen.update()?;
    thread::sleep(time::Duration::from_secs(1));

    return Ok(());
}

