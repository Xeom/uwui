#include <unistd.h>
#include <stdio.h>
#include "public/term.h"

static uwui_term_s  *term;

void fill_region(int x, int y, int w, int h, const char *c)
{
    int xiter, yiter;
    for (xiter = x; xiter < x + w; ++xiter)
    {
        for (yiter = y; yiter < y + h; ++yiter)
            uwui_term_display(term, c, &UWUI_DEFAULT_FONT, xiter, yiter, NULL);
    }
}

void show_border(int w, int h)
{
    fill_region(1,     0,     w - 2, 1,     "-");
    fill_region(0,     1,     1,     h - 2, "|");
    fill_region(1,     h - 1, w - 2, 1,     "-");
    fill_region(w - 1, 1,     1,     h - 2, "|");

    uwui_term_display(term, "*", &UWUI_DEFAULT_FONT, 0,     0,     NULL);
    uwui_term_display(term, "*", &UWUI_DEFAULT_FONT, w - 1, 0,     NULL);
    uwui_term_display(term, "*", &UWUI_DEFAULT_FONT, w - 1, h - 1, NULL);
    uwui_term_display(term, "*", &UWUI_DEFAULT_FONT, 0,     h - 1, NULL);
}

int main(int argc, const char **argv)
{
    int w, h;
    uwui_term_state_s state;
    UWUI_ERRMSG_DECLARE(errmsg);

    term = uwui_term_create(STDIN_FILENO, STDOUT_FILENO, errmsg);

    if (!term)
    {
        fprintf(stderr, "Could not create terminal: %s", errmsg);
        return -1;
    }
    uwui_term_get_state(term, &state);
    state.flags |=  (UWUI_TERM_FLAG_ALT_SCR);
    state.flags &= ~(UWUI_TERM_FLAG_SHOW_CUR);
    if (uwui_term_set_state(term, &state, errmsg) != UWUI_OK)
    {
        fprintf(stderr, "Error setting terminal state: %s", errmsg);
    }

    uwui_term_display(term, "Hello World", &UWUI_DEFAULT_FONT, 1, 1, errmsg);

    uwui_term_get_dims(term, &w, &h);
    show_border(w, h);

    sleep(1);

    uwui_term_destroy(term);

    return 0;
}
