use std::{time, thread};

use lib::pos::Pos;
use lib::chr::Chr;
use lib::cbind::term::Term;
use lib::cbind::font::Font;
use lib::cbind::clib;
use lib::err::ErrorInfo;

fn main() -> Result<(),ErrorInfo>
{
    let term: Term = Term::new()?;

    let mut state = term.get_state();
    state.flags  |= clib::UWUI_TERM_FLAG_ALT_SCR;
    term.set_state(&state)?;

    for (x, c) in "Hello World".chars().enumerate()
    {
        term.display(
            Pos::new(x as i64, 0),
            Chr::new(c, Font::new_default())
        )?;
    }

    thread::sleep(time::Duration::from_secs(1));

    return Ok(());
}
