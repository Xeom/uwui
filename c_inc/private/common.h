#if !defined(UWUI_COMMON_H)
# define UWUI_COMMON_H
# include <stdio.h>
# include <errno.h>
# include <string.h>
# include <stddef.h>
# include "public/common.h"

# define DIM(_x) (sizeof(_x)/sizeof((_x)[0]))

# define TRY(_action) do {         \
        if ((_action) == UWUI_ERR) \
            return UWUI_ERR;       \
    } while (0);

#define _STRIFY(str) #str
#define  STRIFY(str) _STRIFY(str)

#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) > (b) ? (b) : (a))

#define CONTAINER_OF(_ptr, _type, _field) \
    ((_type *)((char *)(_ptr) - offsetof(_type, _field)))

#define ERRMSG_LOC \
    "[" __FILE__ ":" STRIFY(__LINE__) "]: "

#define ERRMSG_FMT \
    ERRMSG_LOC "%s", msg

#define ERRMSG_ERRNO_FMT \
    ERRMSG_LOC "%s: (%d) %s", msg, errno, strerror(errno)

#define ERRMSG(buf, ...) \
    do {                                                \
        char msg[UWUI_ERRMSG_LEN];                      \
        snprintf(msg, sizeof(msg), __VA_ARGS__);        \
        if (buf)                                        \
            snprintf((char *)buf, UWUI_ERRMSG_LEN, ERRMSG_FMT); \
        else                                            \
            fprintf(stderr, ERRMSG_FMT);                \
    } while (0)

#define ERRMSG_ERRNO(buf, ...) \
    do {                                                      \
        char msg[UWUI_ERRMSG_LEN];                            \
        snprintf(msg, sizeof(msg), __VA_ARGS__);              \
        if (buf)                                              \
            snprintf((char *)buf, UWUI_ERRMSG_LEN, ERRMSG_ERRNO_FMT); \
        else                                                  \
            fprintf(stderr, ERRMSG_ERRNO_FMT);                \
    } while (0)

#endif
