#if !defined(UWUI_CONTAINER_LIST_H)
# define UWUI_CONTAINER_LIST_H
# include "private/common.h"

typedef struct uwui_list_item  uwui_list_item_s;
typedef struct uwui_list_range uwui_list_range_s;
typedef struct uwui_list       uwui_list_s;

struct uwui_list_item
{
    uwui_list_item_s *prev, *next;
};

struct uwui_list_range
{
    uwui_list_item_s *first, *last;
};

struct uwui_list
{
    uwui_list_range_s range;
    size_t            len;
};

void uwui_list_init(uwui_list_s *list);

# define UWUI_DECLARE_LIST_CONVERSIONS(T, N, F) \
    static inline T *uwui_list_item_to_ ## N(uwui_list_item_s *item) \
    { return item ? CONTAINER_OF(item, T, F) : NULL; } \
    static inline uwui_list_item_s *uwui_list_item_from_ ## N(T *item) \
    { return &(item->F); } \
    static inline uwui_list_range_s uwui_list_range_from_ ## N(T *first, T *last) \
    { return (uwui_list_range_s){ .first = &first->F, .last = &last->F }; } \
    static inline uwui_list_range_s uwui_list_range_from_single_ ## N(T *item) \
    { return (uwui_list_range_s){ .first = &item->F, .last = &item->F }; } \

size_t uwui_list_len(uwui_list_s *list);

uwui_res_t uwui_list_ins(
    uwui_list_s       *list,
    uwui_list_item_s  *after,
    uwui_list_range_s  toins,
    uwui_errmsg_t      errmsg
);

# define UWUI_DECLARE_LIST_INS(T, N) \
    static inline uwui_res_t uwui_list_ins_ ## N ( \
        uwui_list_s *list, T *after, T *item, uwui_errmsg_t errmsg) \
    { return uwui_list_ins( \
        list, \
        uwui_list_item_from_ ## N(after), \
        uwui_list_range_from_single_ ## N(item), \
        errmsg \
    ); }

uwui_res_t uwui_list_pop(
    uwui_list_s       *list,
    uwui_list_range_s  tocut,
    uwui_errmsg_t      errmsg
);

# define UWUI_DECLARE_LIST_POP(T, N) \
    static inline uwui_res_t uwui_list_pop_ ## N ( \
        uwui_list_s *list, T *item, uwui_errmsg_t errmsg) \
    { return uwui_list_pop(list, uwui_list_range_from_single_ ## N(item), errmsg); }

uwui_list_item_s *uwui_list_get(
    uwui_list_s      *list,
    uwui_list_item_s *item,
    long              rel
);

# define UWUI_DECLARE_LIST_GET(T, N) \
    static inline T *uwui_list_get_ ## N (uwui_list_s *list, T *item, long rel) \
    { return uwui_list_item_to_ ## N( \
        uwui_list_get(list, uwui_list_item_from_ ## N(item), rel)); }

uwui_list_item_s *uwui_list_first(uwui_list_s *list);

# define UWUI_DECLARE_LIST_FIRST(T, N) \
    static inline T *uwui_list_first_ ## N (uwui_list_s *list) \
    { return uwui_list_item_to_ ## N(uwui_list_first(list)); }

uwui_list_item_s *uwui_list_last(uwui_list_s *list);

# define UWUI_DECLARE_LIST_LAST(T, N) \
    static inline T *uwui_list_last_ ## N (uwui_list_s *list) \
    { return uwui_list_item_to_ ## N(uwui_list_last(list)); }

static inline uwui_list_range_s uwui_list_item_to_range(
    uwui_list_item_s *first, uwui_list_item_s *last
)
{
    return (uwui_list_range_s){ .first = first, .last = last };
}

# define UWUI_DECLARE_LIST_TO_RANGE(T, N) \
    static inline uwui_list_range_s uwui_list_ ## N ## _to_range(T *f, T *l) \
    { return uwui_list_item_to_range( \
        uwui_list_item_from_ ## N(f), uwui_list_item_from_ ## N(l)); }

# define UWUI_DECLARE_LIST_METHODS(type, name, field) \
    UWUI_DECLARE_LIST_CONVERSIONS(type, name, field) \
    UWUI_DECLARE_LIST_GET(type, name) \
    UWUI_DECLARE_LIST_POP(type, name) \
    UWUI_DECLARE_LIST_INS(type, name) \
    UWUI_DECLARE_LIST_FIRST(type, name) \
    UWUI_DECLARE_LIST_LAST(type, name) \
    UWUI_DECLARE_LIST_TO_RANGE(type, name)

#endif
