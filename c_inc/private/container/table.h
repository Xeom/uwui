#if !defined(UWUI_CONTAINER_TABLE_H)
# define UWUI_CONTAINER_TABLE_H
# include "private/container/vec.h"
# include "private/container/list.h"
# include <stdbool.h>

typedef struct uwui_table_iterator uwui_table_iterator_s;
typedef struct uwui_bucket_item    uwui_bucket_item_s;
typedef struct uwui_table          uwui_table_s;

struct uwui_table
{
    size_t len;
    uwui_vec_s buckets;
};

struct uwui_table_iterator
{
    size_t              bucketind;
    uwui_bucket_item_s *item;
};

#define TABLE_DEFAULT_SIZE 256
#define UWUI_TABLE_ITERATOR (uwui_table_iterator_s){ 0, NULL }

uwui_res_t uwui_table_init(
    uwui_table_s *table,
    size_t        nbuckets,
    uwui_errmsg_t errmsg
);

void uwui_table_kill(
    uwui_table_s *table
);

size_t uwui_table_len(
    uwui_table_s *table
);

size_t uwui_table_nbuckets(
    uwui_table_s *table
);

uwui_res_t uwui_table_resize(
    uwui_table_s *table,
    size_t        nbuckets,
    uwui_errmsg_t errmsg
);

uwui_res_t uwui_table_del(
    uwui_table_s *table,
    const void   *key,
    size_t        keylen,
    uwui_errmsg_t errmsg
);

bool uwui_table_contains(
    uwui_table_s *table,
    const void   *key,
    size_t        keylen
);

uwui_res_t uwui_table_set(
    uwui_table_s *table,
    const void   *key,
    size_t        keylen,
    const void   *val,
    size_t        vallen,
    uwui_errmsg_t errmsg
);

void uwui_table_get(
    uwui_table_s *table,
    const void   *key,
    size_t        keylen,
    void        **val,
    size_t       *vallen
);

bool uwui_table_iterator_next(
    uwui_table_s          *table,
    uwui_table_iterator_s *iterator,
    void                 **key,
    size_t                *keylen,
    void                 **val,
    size_t                *vallen
);

#endif
