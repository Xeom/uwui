#if !defined(UWUI_CONTAINER_BUCKET_H)
# define UWUI_CONTAINER_BUCKET_H
# include "private/container/list.h"
# include <stdbool.h>

typedef struct uwui_table_iterator uwui_table_iterator_s;
typedef struct uwui_bucket         uwui_bucket_s;
typedef struct uwui_bucket_item    uwui_bucket_item_s;

struct uwui_bucket
{
    uwui_list_s items;
};

void uwui_bucket_init(
    uwui_bucket_s *bucket
);

void uwui_bucket_kill(
    uwui_bucket_s *bucket
);

uwui_res_t uwui_bucket_insert(
    uwui_bucket_s *bucket,
    const void    *key,
    size_t         keylen,
    const void    *val,
    size_t         vallen,
    uwui_errmsg_t  errmsg
);

uwui_res_t uwui_bucket_del(
    uwui_bucket_s *bucket,
    const void    *key,
    size_t         keylen,
    uwui_errmsg_t  errmsg
);

void uwui_bucket_get(
    uwui_bucket_s *table,
    const void    *key,
    size_t         keylen,
    void         **val,
    size_t        *vallen
);

bool uwui_bucket_iterator_next(
    uwui_bucket_s         *bucket,
    uwui_table_iterator_s *iterator,
    void                 **key,
    size_t                *keylen,
    void                 **val,
    size_t                *vallen
);

#endif
