#if !defined(UWUI_CONTAINER_VEC_H)
# define UWUI_CONTAINER_VEC_H
# include <stddef.h>
# include "private/common.h"

typedef struct uwui_vec uwui_vec_s;

struct uwui_vec
{
    size_t width;
    size_t capacity;
    size_t used;

    char *data;
};

#define VEC_INITIALIZER(T) { \
    .width = sizeof(T), .capacity = 0, .used = 0, .data = NULL }

#define VEC_FOREACH(_vec, _type, _name) \
    _type _name; \
    for (size_t _ind = 0, \
         _len = uwui_vec_len(_vec); \
         _name = uwui_vec_get(_vec, _ind), \
         _ind < _len; \
         ++_ind)

void uwui_vec_init(uwui_vec_s *vec, size_t width);

void uwui_vec_kill(uwui_vec_s *vec);

uwui_res_t uwui_vec_ins(
    uwui_vec_s   *vec,
    size_t        ind,
    size_t        n,
    const void   *data,
    uwui_errmsg_t errmsg
);

uwui_res_t uwui_vec_del(
    uwui_vec_s   *vec,
    size_t        ind,
    size_t        n,
    uwui_errmsg_t errmsg
);

void *uwui_vec_get(uwui_vec_s *vec, size_t ind);
const void *uwui_vec_get_const(const uwui_vec_s *vec, size_t ind);

size_t uwui_vec_len(const uwui_vec_s *vec);

static inline uwui_res_t uwui_vec_clr(uwui_vec_s *vec, uwui_errmsg_t errmsg)
{
    return uwui_vec_del(vec, 0, uwui_vec_len(vec), errmsg);
}

static inline uwui_res_t uwui_vec_add(
    uwui_vec_s   *vec,
    size_t        n,
    const void   *data,
    uwui_errmsg_t errmsg
)
{
    return uwui_vec_ins(vec, uwui_vec_len(vec), n, data, errmsg);
}

static inline uwui_res_t uwui_vec_cpy(
    uwui_vec_s   *vec,
    uwui_vec_s   *oth,
    uwui_errmsg_t errmsg
)
{
    return uwui_vec_add(vec, uwui_vec_len(oth), uwui_vec_get(oth, 0), errmsg);
}

#endif
