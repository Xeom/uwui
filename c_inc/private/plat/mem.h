#if !defined(UWUI_PLAT_MEM_H)
# define UWUI_PLAT_MEM_H
# include "private/common.h"

uwui_res_t uwui_alloc(void **res, size_t len, uwui_errmsg_t errmsg);

void uwui_free(void *mem);

#endif
