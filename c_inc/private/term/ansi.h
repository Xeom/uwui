#if !defined(UWUI_TERM_FLAGS_H)
# define UWUI_TERM_FLAGS_H
# include <stdbool.h>
# include <termios.h>
# include "public/term.h"

typedef struct uwui_term uwui_term_s;

uwui_res_t uwui_term_update_flags(
    uwui_term_s     *term,
    uint32_t         new,
    uwui_errmsg_t    errmsg
);

uwui_res_t uwui_term_move_cur(
    uwui_term_s  *term,
    int           col,
    int           row,
    uwui_errmsg_t errmsg
);

uwui_res_t uwui_term_write_str(
    uwui_term_s   *term,
    const char    *str,
    uwui_errmsg_t  errmsg 
);

#endif
