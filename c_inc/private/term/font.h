#if !defined(UWUI_TERM_FONT_H)
# define UWUI_TERM_FONT_H
# include <stdint.h>
# include "private/common.h"
# include "public/term.h"

#define UWUI_COLOUR_16(num, bright) ( \
        (UWUI_COLOUR_FAMILY_16)               | \
        ((num) & UWUI_COLOUR_16_MASK)         | \
        ((bright) ? UWUI_COLOUR_16_BRIGHT : 0)  \
    )

#define UWUI_COLOUR_256(num) ( \
        (UWUI_COLOUR_FAMILY_256)       | \
        ((num) & UWUI_COLOUR_256_MASK)   \
    )

#define UWUI_COLOUR_24BIT(r, g, b) (     \
        (UWUI_COLOUR_FAMILY_24BIT)               | \
        (((r) << 0)  & UWUI_COLOUR_24BIT_R_MASK) | \
        (((g) << 8)  & UWUI_COLOUR_24BIT_G_MASK) | \
        (((b) << 16) & UWUI_COLOUR_24BIT_B_MASK)   \
    )

#define UWUI_COLOUR_GREY_SHADE(shade) \
    COLOUR_256((232 + (shade)) % 24)

typedef enum
{
    UWUI_COLOUR_FAMILY_NONE    = 0x00000000,
    UWUI_COLOUR_FAMILY_16      = 0x01000000,
    UWUI_COLOUR_FAMILY_256     = 0x02000000,
    UWUI_COLOUR_FAMILY_24BIT   = 0x03000000,

    UWUI_COLOUR_FAMILY_MASK    = 0xff000000,

    UWUI_COLOUR_24BIT_R_MASK   = 0x000000ff,
    UWUI_COLOUR_24BIT_G_MASK   = 0x0000ff00,
    UWUI_COLOUR_24BIT_B_MASK   = 0x00ff0000,

    UWUI_COLOUR_256_MASK       = 0x000000ff,

    UWUI_COLOUR_16_MASK        = 0x00000007,
    UWUI_COLOUR_16_BRIGHT      = 0x00000008,

    UWUI_COLOUR_DEFAULT        = UWUI_COLOUR_FAMILY_NONE | 0,

    UWUI_COLOUR_BLACK          = UWUI_COLOUR_16(0, 0),
    UWUI_COLOUR_RED            = UWUI_COLOUR_16(1, 0),
    UWUI_COLOUR_GREEN          = UWUI_COLOUR_16(2, 0),
    UWUI_COLOUR_YELLOW         = UWUI_COLOUR_16(3, 0),
    UWUI_COLOUR_BLUE           = UWUI_COLOUR_16(4, 0),
    UWUI_COLOUR_MAGENTA        = UWUI_COLOUR_16(5, 0),
    UWUI_COLOUR_CYAN           = UWUI_COLOUR_16(6, 0),
    UWUI_COLOUR_WHITE          = UWUI_COLOUR_16(7, 0),

    UWUI_COLOUR_BBLACK         = UWUI_COLOUR_16(0, 1),
    UWUI_COLOUR_BRED           = UWUI_COLOUR_16(1, 1),
    UWUI_COLOUR_BGREEN         = UWUI_COLOUR_16(2, 1),
    UWUI_COLOUR_BYELLOW        = UWUI_COLOUR_16(3, 1),
    UWUI_COLOUR_BBLUE          = UWUI_COLOUR_16(4, 1),
    UWUI_COLOUR_BMAGENTA       = UWUI_COLOUR_16(5, 1),
    UWUI_COLOUR_BCYAN          = UWUI_COLOUR_16(6, 1),
    UWUI_COLOUR_BWHITE         = UWUI_COLOUR_16(7, 1),

    UWUI_COLOUR_ORANGE         = UWUI_COLOUR_256(202),
    UWUI_COLOUR_VIOLET         = UWUI_COLOUR_256(54)
} uwui_colour_t;

uwui_res_t uwui_term_set_font(
    uwui_term_s       *term,
    const uwui_font_s *font,
    uwui_errmsg_t      errmsg
);

#endif
