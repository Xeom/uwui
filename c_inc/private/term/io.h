#if !defined(UWUI_TERM_IO_H)
# define UWUI_TERM_IO_H
# include "private/common.h"

typedef struct uwui_term uwui_term_s;

uwui_res_t uwui_term_write(
    uwui_term_s  *term,
    const char   *data,
    size_t        len,
    uwui_errmsg_t errmsg
);

uwui_res_t uwui_term_writef(
    uwui_term_s  *term,
    uwui_errmsg_t errmsg,
    const char   *fmt,
    ...
);

uwui_res_t uwui_term_read(
    uwui_term_s  *term,
    char         *data,
    size_t       *len,
    uwui_errmsg_t errmsg
);


#endif
