#if !defined(UWUI_TERM_TERM_H)
# define UWUI_TERM_TERM_H
# include <stddef.h>
# include "private/common.h"
# include "public/term.h"
# include "private/term/ansi.h"
# include "private/term/font.h"
# include "private/term/io.h"

# define ESC "\033"
# define CSI "\033["

typedef struct uwui_term       uwui_term_s;
typedef struct uwui_term_state uwui_term_state_s;

struct uwui_term
{
    uint32_t          flags;
    uwui_term_state_s restorestate;
    uwui_font_s       font;
    int               infd, outfd;
    int               currow, curcol;
};

#endif
