#if !defined(UWUI_EVENT_PRODUCER_H)
# define UWUI_EVENT_PRODUCER_H
# include "public/event.h"

struct uwui_event_producer
{
    uwui_event_ctx_s *ctx;
};

uwui_res_t uwui_event_producer_init(
    uwui_event_producer_s  *producer,
    uwui_event_ctx_s       *ctx,
    uwui_errmsg_t           errmsg
);

void uwui_event_producer_destroy(
    uwui_event_producer_s *producer
);

#endif
