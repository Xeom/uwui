#if !defined(UWUI_EVENT_CTX_H)
# define UWUI_EVENT_CTX_H
# include "private/common.h"
# include "private/container/list.h"
# include "private/container/table.h"
# include "public/event.h"

struct uwui_event_ctx
{
    uwui_list_s  queue;
    uwui_table_s consumers;
};

uwui_event_ctx_s *uwui_event_ctx_create(
    uwui_errmsg_t errmsg
);

void uwui_event_ctx_destroy(
    uwui_event_ctx_s *ctx
);

uwui_res_t uwui_event_ctx_push_event(
    uwui_event_ctx_s *ctx,
    uwui_event_s     *event,
    uwui_errmsg_t     errmsg
);

uwui_res_t uwui_event_ctx_add_consumer(
    uwui_event_ctx_s        *ctx,
    uwui_event_consumer_s   *consumer,
    uwui_errmsg_t            errmsg
);

uwui_res_t uwui_event_ctx_rem_consumer(
    uwui_event_ctx_s        *ctx,
    uwui_event_consumer_s   *consumer,
    uwui_errmsg_t            errmsg
);

#endif
