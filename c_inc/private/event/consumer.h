#if !defined(UWUI_EVENT_CONSUMER_H)
# define UWUI_EVENT_CONSUMER_H
# include "public/event.h"
# include "private/container/list.h"

struct uwui_event_consumer
{
    uwui_list_item_s  listitem;
    uwui_event_spec_s spec;
    uwui_event_ctx_s *ctx;
    void             *user;
    uwui_res_t      (*cb)(void *, const uwui_event_s *, uwui_errmsg_t);
};

UWUI_DECLARE_LIST_METHODS(uwui_event_consumer_s, event_consumer, listitem)

uwui_res_t uwui_event_consumer_init(
    uwui_event_consumer_s   *consumer,
    uwui_event_ctx_s        *ctx,
    const uwui_event_spec_s *spec,
    uwui_errmsg_t            errmsg
);

void uwui_event_consumer_kill(
    uwui_event_consumer_s *consumer
);

uwui_event_consumer_s *uwui_event_consumer_create(
    uwui_event_ctx_s        *ctx,
    const uwui_event_spec_s *spec,
    uwui_errmsg_t            errmsg
);

void uwui_event_consumer_destroy(
    uwui_event_consumer_s *consumer
);

uwui_res_t uwui_event_consumer_consume(
    uwui_event_consumer_s *consumer,
    uwui_event_s          *event,
    uwui_errmsg_t          errmsg
);

#endif
