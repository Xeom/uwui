#if !defined(UWUI_EVENT_EVENT_H)
# define UWUI_EVENT_EVENT_H
# include "private/common.h"
# include "private/container/vec.h"
# include "private/container/list.h"
# include "private/container/table.h"
# include "public/event.h"

uwui_event_spec_s uwui_event_spec_sanitize(
    const uwui_event_spec_s *spec
);

struct uwui_event
{
    uwui_list_item_s       listitem;
    uwui_event_ctx_s      *ctx;
    uwui_event_producer_s *producer;
    uwui_event_type_t      type;
    uwui_vec_s             data;
};

UWUI_DECLARE_LIST_METHODS(uwui_event_s, event, listitem)

uwui_res_t uwui_event_init(
    uwui_event_s          *event,
    uwui_event_producer_s *producer,
    uwui_event_type_t      type,
    const uint8_t         *data,
    size_t                 len,
    uwui_errmsg_t          errmsg
);

void uwui_event_kill(
    uwui_event_s *event
);

uwui_event_s *uwui_event_create(
    uwui_event_producer_s *producer,
    uwui_event_type_t      type,
    const uint8_t         *data,
    size_t                 len,
    uwui_errmsg_t          errmsg
);

void uwui_event_destroy(
    uwui_event_s *event
);
#endif
