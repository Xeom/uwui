#if !defined(UWUI_PUBLIC_COMMON_H)
# define UWUI_PUBLIC_COMMON_H

# define UWUI_ERRMSG_LEN 256

# define UWUI_STRIFY_(_s) #_s
# define UWUI_STRIFY(_s)  UWUI_STRIFY_(_s)

# define UWUI_ERRMSG_DECLARE(errmsg) \
    uwui_errmsg_t errmsg = \
        "<NO ERRMSG " __FILE__ ":" UWUI_STRIFY(__LINE__) ">"

typedef enum
{
    UWUI_OK  =  0,
    UWUI_ERR = -1
} uwui_res_t;

typedef unsigned char uwui_errmsg_t[UWUI_ERRMSG_LEN];

#endif
