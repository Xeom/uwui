#if !defined(UWUI_PUBLIC_TERM_H)
# define UWUI_PUBLIC_TERM_H
# include <termios.h>
# include <stdbool.h>
# include <stdint.h>
# include "common.h"

typedef struct uwui_term       uwui_term_s;
typedef struct uwui_term_state uwui_term_state_s;

typedef struct uwui_font       uwui_font_s;

#define UWUI_DEFAULT_FONT (uwui_font_s) {  \
        .attrs = 0,              \
        .fg    = uwui_colour_default(), \
        .bg    = uwui_colour_default()  \
    }

# define UWUI_FONT_BOLD         0x0001
# define UWUI_FONT_FAINT        0x0002
# define UWUI_FONT_ITALIC       0x0004
# define UWUI_FONT_UNDER        0x0008
# define UWUI_FONT_BLINK        0x0010
# define UWUI_FONT_FAST_BLINK   0x0020
# define UWUI_FONT_REV          0x0040
# define UWUI_FONT_HIDE         0x0080
# define UWUI_FONT_STRIKE       0x0100

struct uwui_font
{
    uint16_t attrs;

    uint32_t fg;
    uint32_t bg;
};

# define UWUI_TERM_FLAG_SHOW_CUR   0x0001
# define UWUI_TERM_FLAG_PASTE_BRA  0x0002
# define UWUI_TERM_FLAG_KEYPAD     0x0004
# define UWUI_TERM_FLAG_ALT_SCR    0x0008

struct uwui_term_state
{
    uint32_t flags;
    struct termios   attrs;
};

uwui_term_s *uwui_term_create(
    int           infd,
    int           outfd,
    uwui_errmsg_t errmsg
);

void uwui_term_destroy(
    uwui_term_s  *term
);

uwui_res_t uwui_term_set_state(
    uwui_term_s             *term,
    const uwui_term_state_s *state,
    uwui_errmsg_t            errmsg
);

void uwui_term_get_state(
    const uwui_term_s *term,
    uwui_term_state_s *state
);

uwui_res_t uwui_term_display(
    uwui_term_s        *term,
    const char         *str,
    const uwui_font_s  *font,
    int                 col,
    int                 row,
    uwui_errmsg_t       errmsg
);

uwui_res_t uwui_term_clear_screen(
    uwui_term_s   *term,
    uwui_errmsg_t  errmsg
);

void uwui_term_get_dims(
    uwui_term_s    *term,
    unsigned       *w,
    unsigned       *h
);

uint32_t uwui_colour_default(void);

uint32_t uwui_colour_16(uint8_t num, bool bright);

uint32_t uwui_colour_256(uint8_t num);

uint32_t uwui_colour_24bit(uint8_t r, uint8_t g, uint8_t b);

#endif
