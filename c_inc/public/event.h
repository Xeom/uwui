#if !defined(UWUI_PUBLIC_EVENT_H)
# define UWUI_PUBLIC_EVENT_H
# include <stdint.h>
# include <stddef.h>
# include "common.h"
# include "eventtypes.h"

typedef struct uwui_event              uwui_event_s;
typedef struct uwui_event_ctx          uwui_event_ctx_s;
typedef struct uwui_event_spec         uwui_event_spec_s;
typedef struct uwui_event_producer     uwui_event_producer_s;
typedef struct uwui_event_consumer     uwui_event_consumer_s;

struct uwui_event_spec
{
    const uwui_event_producer_s *producer;
    uwui_event_type_t            typ;
};

uwui_event_ctx_s *uwui_event_ctx_create(
    uwui_errmsg_t      errmsg
);

void uwui_event_ctx_destroy(
    uwui_event_ctx_s *ctx
);

uwui_res_t uwui_event_push(
    uwui_event_producer_s *producer,
    uwui_event_type_t      type,
    const uint8_t         *data,
    size_t                 len,
    uwui_errmsg_t          errmsg
);

uwui_event_consumer_s *uwui_event_consumer_create(
    uwui_event_ctx_s        *ctx,
    const uwui_event_spec_s *spec,
    uwui_errmsg_t            errmsg
);

uwui_res_t uwui_event_consumer_set_cb(
    uwui_event_consumer_s *consumer,
    void                  *user,
    uwui_res_t           (*cb)(void *, const uwui_event_s *, uwui_errmsg_t),
    uwui_errmsg_t          errmsg
);

void uwui_event_consumer_destroy(
    uwui_event_consumer_s *consumer
);

uwui_event_producer_s *uwui_event_producer_create(
    uwui_event_ctx_s       *ctx,
    uwui_errmsg_t           errmsg
);

void uwui_event_producer_destroy(
    uwui_event_producer_s *producer
);

uwui_res_t uwui_event_ctx_dispatch_next(
    uwui_event_ctx_s *ctx,
    uwui_errmsg_t     errmsg
);

const uint8_t *uwui_event_get_data(
    const uwui_event_s *event
);

size_t uwui_event_get_data_len(
    const uwui_event_s *event
);

uwui_event_type_t uwui_event_get_type(
    const uwui_event_s *event
);

#endif
